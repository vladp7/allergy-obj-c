//
//  AddAllergenTextField.m
//  Allergy
//
//  Created by Владислав on 25.01.17.
//  Copyright © 2017 Vladislav Popov. All rights reserved.
//

#import "AddAllergenTextField.h"

@implementation AddAllergenTextField

- (CGRect)textRectForBounds:(CGRect)bounds {
    return [self rectForBounds:bounds];
}

- (CGRect)editingRectForBounds:(CGRect)bounds {
    return [self rectForBounds:bounds];
}

- (CGRect)placeholderRectForBounds:(CGRect)bounds {
    return [self rectForBounds:bounds];
}

//here 40 - is your x offset
- (CGRect)rectForBounds:(CGRect)bounds {
    return CGRectInset(bounds, 32, 29);
}

@end
