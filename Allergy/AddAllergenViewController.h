//
//  AddAllergenViewController.h
//  Allergy
//
//  Created by Владислав on 13.01.17.
//  Copyright © 2017 Vladislav Popov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddAllergenViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *allergenTextField;

@end
