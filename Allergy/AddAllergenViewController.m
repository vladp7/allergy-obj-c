//
//  AddAllergenViewController.m
//  Allergy
//
//  Created by Владислав on 13.01.17.
//  Copyright © 2017 Vladislav Popov. All rights reserved.
//

#import "AddAllergenViewController.h"

@interface AddAllergenViewController () <UITextFieldDelegate>

@property (strong, nonatomic) NSMutableArray* allergensArray;
@property (strong, nonatomic) NSMutableArray* allergensStatementsArray;

@end

@implementation AddAllergenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.allergensArray = [[NSMutableArray alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma  mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.allergenTextField resignFirstResponder];
    return YES;
}

#pragma mark - Actions
- (IBAction)onClose:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)saveBtn:(id)sender {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
//    self.allergensArray = [[NSMutableArray alloc] init];
    
    self.allergensArray =
   [NSMutableArray arrayWithArray:[userDefaults arrayForKey:@"myAllergens"]];
    self.allergensStatementsArray = [NSMutableArray arrayWithArray:[userDefaults arrayForKey:@"myAllergensStatements"]];
//    
//    MyAllergen *allergen = [[MyAllergen alloc] init];
//    allergen.allergenName = self.allergenTextField.text;
//    allergen.statement = NO;
    
    NSString * textFieldText = self.allergenTextField.text;
    
    [self.allergensArray addObject:textFieldText];
//    [self.allergensArray addObject:allergen];
    [self.allergensStatementsArray addObject:[NSNumber numberWithInt:0]];
    
    [userDefaults setObject:self.allergensArray forKey:@"myAllergens"];
    [userDefaults setObject:self.allergensStatementsArray forKey:@"myAllergensStatements"];
    
    [self dismissViewControllerAnimated:NO completion:nil];
}

@end
