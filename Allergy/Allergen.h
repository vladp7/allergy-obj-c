//
//  Allergen.h
//  Allergy
//
//  Created by Владислав on 12.01.17.
//  Copyright © 2017 Vladislav Popov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Allergen : NSObject

@property (strong, nonatomic) NSString* allergenGroup;
@property (strong, nonatomic) NSArray* allergensArray;
@property (strong, nonatomic) NSArray* allergensArrayEng;
@property (strong, nonatomic) NSArray* imagesArray;

@end
