//
//  Allergens.h
//  Allergy
//
//  Created by Владислав on 12.01.17.
//  Copyright © 2017 Vladislav Popov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Allergens : NSObject

- (NSArray*) allergensArray;

@end
