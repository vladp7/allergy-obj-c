//
//  Allergens.m
//  Allergy
//
//  Created by Владислав on 12.01.17.
//  Copyright © 2017 Vladislav Popov. All rights reserved.
//

#import "Allergens.h"
#import "Allergen.h"

@implementation Allergens

- (NSArray*) allergensArray {
    
//    allergensDict = @{@"ФРУКТЫ, ЯГОДЫ" : @[@"Ананас", @"Персик", @"Голубика", @"Апельсин", @"Дыня", @"Белый виноград", @"Лимон", @"Груша", @"Земляника", @"Грейпфрут", @"Яблоки", @"Слива", @"Банан", @"Авокадо"],
//                      @"ОВОЩИ" : @[@"Морковь", @"Оливки", @"Капуста брокколи", @"Баклажан", @"Фасоль пятнистая", @"Капуста кочанная", @"Свекла", @"Фасоль стручковая", @"Капуста цветная", @"Томаты", @"Горошек зеленый", @"Чеснок", @"Перец сладкий", @"Бобы соевые", @"Лук", @"Тыква", @"Картофель", @"Огурец"],
//                      @"КРУПЫ" : @[@"Овес", @"Пшено", @"Рожь", @"Пшеница", @"Ячмень", @"Кукуруза", @"Греча", @"Рис", @"Глютен"],
//                      @"МЯСО, ПТИЦА" : @[@"Говядина", @"Баранина", @"Курица", @"Свинина", @"Кролик", @"Индейка"],
//                      @"РЫБА, МОРЕПРОДУКТЫ" : @[@"Палтус", @"Тунец", @"Краб", @"Сардины", @"Форель", @"Креветки", @"Камбала", @"Хек", @"Устрицы", @"Треска", @"Лосось", @"Кальмары"],
//                      @"ПРЯНОСТИ, СПЕЦИИ" : @[@"Перец черный", @"Петрушка", @"Сельдерей", @"Перец чили"],
//                      @"ЯЙЦО, МОЛОЧНЫЕ ПРОДУКТЫ" : @[@"Яичный белок", @"Йогурт", @"Сыр мягкий", @"Яичный желток", @"Масло сливочное", @"Сыр брынза", @"Молоко коровье", @"Бета-лактоглобулин", @"Сыр чеддер", @"Молоко козье", @"Казеин", @"Сыр швейцарский"],
//                      @"ОРЕХИ, СЕМЕЧКИ" : @[@"Арахис", @"Орехи грецкие", @"Семя подсолнечника", @"Орех колы", @"Миндаль", @"Кунжут"],
//                      @"ПРОЧЕЕ" : @[@"Грибы (шампиньоны)", @"Табак", @"Сахар тростниковый", @"Дрожжи пекарские", @"Кофе", @"Шоколад", @"Дрожжи пивные", @"Чай черный", @"Мед"],
//                      @"МОИ АЛЛЕРГЕНЫ" : @[@"Нет добавленных"]};
    
    Allergen * fruitsAndBerries = [[Allergen alloc] init];
    Allergen * vegetables = [[Allergen alloc] init];
    Allergen * cereals = [[Allergen alloc] init];
    Allergen * meatAndPoultry = [[Allergen alloc] init];
    Allergen * fishAndSeafood = [[Allergen alloc] init];
    Allergen * herbsAndSpices = [[Allergen alloc] init];
    Allergen * eggAndMilkProducts = [[Allergen alloc] init];
    Allergen * nutsAndSeeds = [[Allergen alloc] init];
    Allergen * other = [[Allergen alloc] init];
    Allergen * myAllergens = [[Allergen alloc] init];
    
    
    
    
    fruitsAndBerries.allergenGroup = @"ФРУКТЫ, ЯГОДЫ";
    fruitsAndBerries.allergensArray = @[@"Ананас", @"Персик", @"Голубика", @"Апельсин", @"Дыня", @"Белый виноград", @"Лимон", @"Груша", @"Земляника", @"Грейпфрут", @"Яблоки", @"Слива", @"Банан", @"Авокадо"];
    fruitsAndBerries.allergensArrayEng = @[@"Ananas", @"Persik", @"Golubika", @"Apelsin", @"Dyinya", @"Belyiy vinograd", @"Limon", @"Grusha", @"Zemlyanika", @"Greypfrut", @"Yabloki", @"Sliva", @"Banan", @"Avokado"];
    fruitsAndBerries.imagesArray = @[@"eggs", @"eggs", @"eggs", @"eggs", @"eggs", @"eggs", @"eggs", @"eggs", @"eggs", @"eggs", @"eggs", @"eggs", @"eggs", @"eggs"];
    
    vegetables.allergenGroup = @"ОВОЩИ";
    vegetables.allergensArray = @[@"Морковь", @"Оливки", @"Капуста брокколи", @"Баклажан", @"Фасоль пятнистая", @"Капуста кочанная", @"Свекла", @"Фасоль стручковая", @"Капуста цветная", @"Томаты", @"Горошек зеленый", @"Чеснок", @"Перец сладкий", @"Бобы соевые", @"Лук", @"Тыква", @"Картофель", @"Огурец"];
    vegetables.allergensArrayEng = @[@"Morkov", @"Olivki", @"Kapusta brokkoli", @"Baklazhan", @"Fasol pyatnistaya", @"Kapusta kochannaya", @"Svekla", @"Fasol struchkovaya", @"Kapusta tsvetnaya", @"Tomatyi", @"Goroshek zelenyiy", @"Chesnok", @"Perets sladkiy", @"Bobyi soevyie", @"Luk", @"Tyikva", @"Kartofel", @"Ogurets"];
    vegetables.imagesArray = @[@"eggs", @"eggs", @"eggs", @"eggs", @"eggs", @"eggs", @"eggs", @"eggs", @"eggs", @"eggs", @"eggs", @"eggs", @"eggs", @"eggs", @"eggs", @"eggs", @"eggs", @"eggs"];
    
    cereals.allergenGroup = @"КРУПЫ";
    cereals.allergensArray = @[@"Овес", @"Пшено", @"Рожь", @"Пшеница", @"Ячмень", @"Кукуруза", @"Греча", @"Рис", @"Глютен"];
    cereals.allergensArrayEng = @[@"Oves", @"Psheno", @"Rozh", @"Pshenitsa", @"Yachmen", @"Kukuruza", @"Grecha", @"Ris", @"Glyuten"];
    cereals.imagesArray = @[@"eggs", @"eggs", @"eggs", @"eggs", @"eggs", @"eggs", @"eggs", @"eggs", @"eggs"];
    
    meatAndPoultry.allergenGroup = @"МЯСО, ПТИЦА";
    meatAndPoultry.allergensArray = @[@"Говядина", @"Баранина", @"Курица", @"Свинина", @"Кролик", @"Индейка"];
    meatAndPoultry.allergensArrayEng = @[@"Govyadina", @"Baranina", @"Kuritsa", @"Svinina", @"Krolik", @"Indeyka"];
    meatAndPoultry.imagesArray = @[@"eggs", @"eggs", @"eggs", @"eggs", @"eggs", @"eggs"];
    
    fishAndSeafood.allergenGroup = @"РЫБА, МОРЕПРОДУКТЫ";
    fishAndSeafood.allergensArray = @[@"Палтус", @"Тунец", @"Краб", @"Сардины", @"Форель", @"Креветки", @"Камбала", @"Хек", @"Устрицы", @"Треска", @"Лосось", @"Кальмары"];
    fishAndSeafood.allergensArrayEng = @[@"Paltus", @"Tunets", @"Krab", @"Sardinyi", @"Forel", @"Krevetki", @"Kambala", @"Hek", @"Ustritsyi", @"Treska", @"Losos", @"Kalmaryi"];
    fishAndSeafood.imagesArray = @[@"eggs", @"eggs", @"eggs", @"eggs", @"eggs", @"eggs", @"eggs", @"eggs", @"eggs", @"eggs", @"eggs", @"eggs"];
    
    herbsAndSpices.allergenGroup = @"ПРЯНОСТИ, СПЕЦИИ";
    herbsAndSpices.allergensArray = @[@"Перец черный", @"Петрушка", @"Сельдерей", @"Перец чили"];
    herbsAndSpices.allergensArrayEng = @[@"Perets chernyiy", @"Petrushka", @"Selderey", @"Perets chili"];
    herbsAndSpices.imagesArray = @[@"eggs", @"eggs", @"eggs", @"eggs"];
    
    eggAndMilkProducts.allergenGroup = @"ЯЙЦО, МОЛОЧНЫЕ ПРОДУКТЫ";
    eggAndMilkProducts.allergensArray = @[@"Яичный белок", @"Йогурт", @"Сыр мягкий", @"Яичный желток", @"Масло сливочное", @"Сыр брынза", @"Молоко коровье", @"Бета-лактоглобулин", @"Сыр чеддер", @"Молоко козье", @"Казеин", @"Сыр швейцарский"];
    eggAndMilkProducts.allergensArrayEng = @[@"Yaichnyiy belok", @"Yogurt", @"Syir myagkiy", @"Yaichnyiy zheltok", @"Maslo slivochnoe", @"Syir bryinza", @"Moloko korove", @"Beta-laktoglobulin", @"Syir chedder", @"Moloko koze", @"Kazein", @"Syir shveytsarskiy"];
    eggAndMilkProducts.imagesArray = @[@"eggs", @"eggs", @"eggs", @"eggs", @"eggs", @"eggs", @"eggs", @"eggs", @"eggs", @"eggs", @"eggs", @"eggs"];
    
    nutsAndSeeds.allergenGroup = @"ОРЕХИ, СЕМЕЧКИ";
    nutsAndSeeds.allergensArray = @[@"Арахис", @"Орехи грецкие", @"Семя подсолнечника", @"Орех колы", @"Миндаль", @"Кунжут"];
    nutsAndSeeds.allergensArrayEng = @[@"Arahis", @"Orehi gretskie", @"Semya podsolnechnika", @"Oreh kolyi", @"Mindal", @"Kunzhut"];
    nutsAndSeeds.imagesArray = @[@"eggs", @"eggs", @"eggs", @"eggs", @"eggs", @"eggs"];
    
    other.allergenGroup = @"ПРОЧЕЕ";
    other.allergensArray = @[@"Грибы (шампиньоны)", @"Табак", @"Сахар тростниковый", @"Дрожжи пекарские", @"Кофе", @"Шоколад", @"Дрожжи пивные", @"Чай черный", @"Мед"];
    other.allergensArrayEng = @[@"Gribyi (shampinonyi)", @"Tabak", @"Sahar trostnikovyiy", @"Drozhzhi pekarskie", @"Kofe", @"Shokolad", @"Drozhzhi pivnyie", @"Chay chernyiy", @"Med"];
    other.imagesArray = @[@"eggs", @"eggs", @"eggs", @"eggs", @"eggs", @"eggs", @"eggs", @"eggs", @"eggs"];
    
//    myAllergens.allergenGroup = @"МОИ АЛЛЕРГЕНЫ";
//    myAllergens.allergensArray = @[@"Нет добавленных"];

    
    NSArray * dataArray = [NSArray arrayWithObjects:fruitsAndBerries, vegetables, cereals, meatAndPoultry, fishAndSeafood, herbsAndSpices, eggAndMilkProducts, nutsAndSeeds, other, myAllergens, nil];
    
    return dataArray;
}

@end
