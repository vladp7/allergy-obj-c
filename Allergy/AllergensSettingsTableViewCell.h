//
//  AllergensSettingsTableViewCell.h
//  Allergy
//
//  Created by Владислав on 17.01.17.
//  Copyright © 2017 Vladislav Popov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AllergensSettingsTableViewCell : UITableViewCell

@property (strong, nonatomic) NSMutableArray* fruitsAndBerriesArray;
@property (strong, nonatomic) NSMutableArray* vegetablesArray;
@property (strong, nonatomic) NSMutableArray* cerealsArray;
@property (strong, nonatomic) NSMutableArray* meatAndPoultryArray;
@property (strong, nonatomic) NSMutableArray* fishAndSeafoodArray;
@property (strong, nonatomic) NSMutableArray* herbsAndSpicesArray;
@property (strong, nonatomic) NSMutableArray* eggAndMilkProductsArray;
@property (strong, nonatomic) NSMutableArray* nutsAndSeedsArray;
@property (strong, nonatomic) NSMutableArray* otherArray;
@property (strong, nonatomic) NSMutableArray* myAllergensArray;



@property (weak, nonatomic) IBOutlet UILabel *allergenLabel;



@property (weak, nonatomic) IBOutlet UIButton *switchBtnFruitsAndBerries;
@property (weak, nonatomic) IBOutlet UIButton *switchBtnVegetables;
@property (weak, nonatomic) IBOutlet UIButton *switchBtnCereals;
@property (weak, nonatomic) IBOutlet UIButton *switchBtnMeatAndPoultry;
@property (weak, nonatomic) IBOutlet UIButton *switchBtnFishAndSeafood;
@property (weak, nonatomic) IBOutlet UIButton *switchBtnHerbsAndSpices;
@property (weak, nonatomic) IBOutlet UIButton *switchBtnEggAndMilkProducts;
@property (weak, nonatomic) IBOutlet UIButton *switchBtnNutsAndSeeds;
@property (weak, nonatomic) IBOutlet UIButton *switchBtnOther;
@property (weak, nonatomic) IBOutlet UIButton *switchBtnMyAllergens;







@end
