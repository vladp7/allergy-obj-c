//
//  AllergensSettingsTableViewCell.m
//  Allergy
//
//  Created by Владислав on 17.01.17.
//  Copyright © 2017 Vladislav Popov. All rights reserved.
//

#import "AllergensSettingsTableViewCell.h"
#import "AllergensStatements.h"

@implementation AllergensSettingsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    NSDictionary *catalogDict = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"catalog"];
    
    self.fruitsAndBerriesArray = [[NSMutableArray alloc] init];
    self.vegetablesArray = [[NSMutableArray alloc] init];
    self.cerealsArray = [[NSMutableArray alloc] init];
    self.meatAndPoultryArray = [[NSMutableArray alloc] init];
    self.fishAndSeafoodArray = [[NSMutableArray alloc] init];
    self.herbsAndSpicesArray = [[NSMutableArray alloc] init];
    self.eggAndMilkProductsArray = [[NSMutableArray alloc] init];
    self.nutsAndSeedsArray = [[NSMutableArray alloc] init];
    self.otherArray = [[NSMutableArray alloc] init];
    
    NSArray* itemsArray = catalogDict[@"items"];
    
    for (int i = 0; i < [itemsArray count]; i++) {
        switch ([[itemsArray objectAtIndex:i][@"_catalog"] intValue])
        {
            case 1:
                
                [self.fruitsAndBerriesArray addObject:[itemsArray objectAtIndex:i]];
                
                break;
                
            case 2:
                
                [self.vegetablesArray addObject:[itemsArray objectAtIndex:i]];
                
                break;
                
            case 3:
                
                [self.cerealsArray addObject:[itemsArray objectAtIndex:i]];
                
                break;
                
            case 4:
                
                [self.meatAndPoultryArray addObject:[itemsArray objectAtIndex:i]];
                
                break;
                
            case 5:
                
                [self.fishAndSeafoodArray addObject:[itemsArray objectAtIndex:i]];
                
                break;
                
            case 6:
                
                [self.herbsAndSpicesArray addObject:[itemsArray objectAtIndex:i]];
                
                break;
                
            case 7:
                
                [self.eggAndMilkProductsArray addObject:[itemsArray objectAtIndex:i]];
                
                break;
                
            case 8:
                
                [self.nutsAndSeedsArray addObject:[itemsArray objectAtIndex:i]];
                
                break;
                
            case 9:
                
                [self.otherArray addObject:[itemsArray objectAtIndex:i]];
                
                break;
                
            default:
                
                break;
        }
    }

    
//    AllergensStatements *allergensStatements = [[AllergensStatements alloc] init];
//    
//    self.fruitsAndBerriesArray = [NSMutableArray arrayWithArray:allergensStatements.fruitsAndBerriesStatementsArray];
    
    //[self.switchBtn setImage:[UIImage imageNamed:@"Icon - Right"] forState:UIControlStateNormal];
    //[self.switchBtn setImage:[UIImage imageNamed:@"Icon - Back"] forState:UIControlStateSelected];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Custom methods
- (void)switchStatementForItemOfCatalogIndex:(NSUInteger)catalogIndex itemIndex:(NSUInteger)itemIndex toStatement:(BOOL)statement {
    
    NSDictionary *structCatalog = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"structCatalog"];
    NSMutableDictionary *rootDict = [NSMutableDictionary dictionaryWithDictionary:structCatalog];
    
    NSMutableArray* catalogsMutableArray = [NSMutableArray arrayWithArray:[rootDict objectForKey:@"catalogs"]];
    
    NSMutableDictionary* catalogsValueDict = [NSMutableDictionary dictionaryWithDictionary:[catalogsMutableArray objectAtIndex:catalogIndex]];
    
    NSMutableArray* itemsMutableArray = [NSMutableArray arrayWithArray:[catalogsValueDict objectForKey:@"items"]];
    
    NSMutableDictionary* itemValueDict = [NSMutableDictionary dictionaryWithDictionary:[itemsMutableArray objectAtIndex:itemIndex]];
    
    [itemValueDict removeObjectForKey:@"statement"];
    [itemValueDict setObject:@(statement) forKey:@"statement"];
    
    itemsMutableArray[itemIndex] = itemValueDict;
    [catalogsValueDict removeObjectForKey:@"items"];
    [catalogsValueDict setObject:itemsMutableArray forKey:@"items"];
    catalogsMutableArray[catalogIndex] = catalogsValueDict;
    [rootDict removeObjectForKey:@"catalogs"];
    [rootDict setObject:catalogsMutableArray forKey:@"catalogs"];
    
    
    //[valueDict setObject:newValueObject forKey:@"key2"];
    
    //[structCatalog release];
    structCatalog = nil;
    structCatalog = [[NSDictionary alloc] initWithDictionary:rootDict];
    
    [[NSUserDefaults standardUserDefaults] setObject:structCatalog forKey:@"structCatalog"];
    
    //NSLog(@"strCat = %@", structCatalog);
    
}

#pragma mark - Actions
- (IBAction)switchBtnFruitsAndBerriesAction:(id)sender {
    
    NSIndexPath *indexPath = [self indexPathBySenderBtn:sender];
    
    //[self saveStatementForSwitchBtn:self.switchBtnFruitsAndBerries by:self.fruitsAndBerriesArray withKey:@"fruitsAndBerriesStatements" by:indexPath];
    
    if (self.switchBtnFruitsAndBerries.selected) {
        [self.switchBtnFruitsAndBerries setSelected:NO];
        
        
        
        
        /*
        NSDictionary *structCatalog = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"structCatalog"];
        NSMutableDictionary *rootDict = [NSMutableDictionary dictionaryWithDictionary:structCatalog];
        
        NSMutableArray* catalogsMutableArray = [NSMutableArray arrayWithArray:[rootDict objectForKey:@"catalogs"]];
        
        NSMutableDictionary* catalogsValueDict = [NSMutableDictionary dictionaryWithDictionary:[catalogsMutableArray objectAtIndex:0]];
        
        NSMutableArray* itemsMutableArray = [NSMutableArray arrayWithArray:[catalogsValueDict objectForKey:@"items"]];
        
        NSMutableDictionary* itemValueDict = [NSMutableDictionary dictionaryWithDictionary:[itemsMutableArray objectAtIndex:indexPath.row]];
        
        [itemValueDict removeObjectForKey:@"statement"];
        [itemValueDict setObject:@NO forKey:@"statement"];
        
        itemsMutableArray[indexPath.row] = itemValueDict;
        [catalogsValueDict removeObjectForKey:@"items"];
        [catalogsValueDict setObject:itemsMutableArray forKey:@"items"];
        catalogsMutableArray[0] = catalogsValueDict;
        [rootDict removeObjectForKey:@"catalogs"];
        [rootDict setObject:catalogsMutableArray forKey:@"catalogs"];
        
        
        //[valueDict setObject:newValueObject forKey:@"key2"];
        
        //[structCatalog release];
        structCatalog = nil;
        structCatalog = [[NSDictionary alloc] initWithDictionary:rootDict];
        
        
        //NSLog(@"strCatOff = %@", structCatalog);
        */
        [self switchStatementForItemOfCatalogIndex:0 itemIndex:indexPath.row toStatement:NO];
        
        
        
        
        
//        self.fruitsAndBerriesArray = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] arrayForKey:@"fruitsAndBerriesStatements"]];
        
//        [self.fruitsAndBerriesArray replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithInt:0]];
        
//        [[NSUserDefaults standardUserDefaults] setObject:self.fruitsAndBerriesArray forKey:@"fruitsAndBerriesStatements"];
        
    } else {
        [self.switchBtnFruitsAndBerries setSelected:YES];
        
        
        
        
        //NSMutableDictionary *structCatalog = [[NSMutableDictionary alloc] init];
//        NSDictionary *structCatalog = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"structCatalog"];
//        NSMutableDictionary *structCatalogMut = [structCatalog mutableCopy];
        
//        NSLog(@"statement before2 = %@", structCatalog[@"catalogs"][0][@"items"][indexPath.row][@"statement"]);
        
//        NSMutableDictionary *allergen = [structCatalog[@"catalogs"][0][@"items"][indexPath.row] mutableCopy];
        
        //[structCatalog[@"catalogs"][0][@"items"][indexPath.row] removeObjectForKey:@"statement"];
//        [allergen removeObjectForKey:@"statement"];
        //[structCatalog[@"catalogs"][0][@"items"][indexPath.row] setObject:@NO forKey:@"statement"];
//        [allergen setObject:@YES forKey:@"statement"];
        
//        [structCatalogMut[@"catalogs"][0][@"items"] replaceObjectAtIndex:indexPath.row withObject:allergen];
        
//        NSLog(@"str cat = %@", structCatalog);
        
        //NSMutableDictionary *structCatalog = [originalAr mutableCopy];
        
        //[[NSUserDefaults standardUserDefaults] setObject:<#(nullable id)#> forKey:<#(nonnull NSString *)#>
        
        
        //NSLog(@"statement after1 = %@", structCatalog[@"catalogs"][0][@"items"][indexPath.row][@"statement"]);
//        NSLog(@"statement after2 = %@", allergen[@"statement"]);
        
        //[[NSUserDefaults standardUserDefaults] setObject:structCatalog forKey:@"structCatalog"];
        /*
        NSMutableDictionary *rootDict = [NSMutableDictionary dictionaryWithDictionary:structCatalog];
        
        NSMutableArray* catalogsMutableArray = [NSMutableArray arrayWithArray:[rootDict objectForKey:@"catalogs"]];
        
        NSMutableDictionary* catalogsValueDict = [NSMutableDictionary dictionaryWithDictionary:[catalogsMutableArray objectAtIndex:0]];
        
        NSMutableArray* itemsMutableArray = [NSMutableArray arrayWithArray:[catalogsValueDict objectForKey:@"items"]];
        
        NSMutableDictionary* itemValueDict = [NSMutableDictionary dictionaryWithDictionary:[itemsMutableArray objectAtIndex:indexPath.row]];
        
        [itemValueDict removeObjectForKey:@"statement"];
        [itemValueDict setObject:@YES forKey:@"statement"];
        
        itemsMutableArray[indexPath.row] = itemValueDict;
        [catalogsValueDict removeObjectForKey:@"items"];
        [catalogsValueDict setObject:itemsMutableArray forKey:@"items"];
        catalogsMutableArray[0] = catalogsValueDict;
        [rootDict removeObjectForKey:@"catalogs"];
        [rootDict setObject:catalogsMutableArray forKey:@"catalogs"];
        
        
        //[valueDict setObject:newValueObject forKey:@"key2"];
        
        //[structCatalog release];
        structCatalog = nil;
        structCatalog = [[NSDictionary alloc] initWithDictionary:rootDict];
        
        
        NSLog(@"strCatOn = %@", structCatalog);
        */
        [self switchStatementForItemOfCatalogIndex:0 itemIndex:indexPath.row toStatement:YES];

        
        
        
        
//        self.fruitsAndBerriesArray = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] arrayForKey:@"fruitsAndBerriesStatements"]];
        
//        [self.fruitsAndBerriesArray replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithInt:1]];
//        [[NSUserDefaults standardUserDefaults] setObject:self.fruitsAndBerriesArray forKey:@"fruitsAndBerriesStatements"];
    }
    
//    [self.fruitsAndBerriesArray[indexPath.row][@"statement"]];
}

- (IBAction)switchBtnVegetablesAction:(id)sender {
    
    NSIndexPath *indexPath = [self indexPathBySenderBtn:sender];
    
    //[self saveStatementForSwitchBtn:self.switchBtnVegetables by:self.vegetablesArray withKey:@"vegetablesStatements" by:indexPath];
    
    if (self.switchBtnVegetables.selected) {
        
        [self.switchBtnVegetables setSelected:NO];
        [self switchStatementForItemOfCatalogIndex:1 itemIndex:indexPath.row toStatement:NO];
        
    } else {
        
        [self.switchBtnVegetables setSelected:YES];
        [self switchStatementForItemOfCatalogIndex:1 itemIndex:indexPath.row toStatement:YES];
    }
}

- (IBAction)switchBtnCerealsAction:(id)sender {
    
    NSIndexPath *indexPath = [self indexPathBySenderBtn:sender];
    
    //[self saveStatementForSwitchBtn:self.switchBtnCereals by:self.cerealsArray withKey:@"cerealsStatements" by:indexPath];
    
    if (self.switchBtnCereals.selected) {
        
        [self.switchBtnCereals setSelected:NO];
        [self switchStatementForItemOfCatalogIndex:2 itemIndex:indexPath.row toStatement:NO];
        
    } else {
        
        [self.switchBtnCereals setSelected:YES];
        [self switchStatementForItemOfCatalogIndex:2 itemIndex:indexPath.row toStatement:YES];
    }
}

- (IBAction)switchBtnMeatAndPoultryAction:(id)sender {
    
    NSIndexPath *indexPath = [self indexPathBySenderBtn:sender];
    
    //[self saveStatementForSwitchBtn:self.switchBtnMeatAndPoultry by:self.meatAndPoultryArray withKey:@"meatAndPoultryStatements" by:indexPath];
    if (self.switchBtnMeatAndPoultry.selected) {
        
        [self.switchBtnMeatAndPoultry setSelected:NO];
        [self switchStatementForItemOfCatalogIndex:3 itemIndex:indexPath.row toStatement:NO];
        
    } else {
        
        [self.switchBtnMeatAndPoultry setSelected:YES];
        [self switchStatementForItemOfCatalogIndex:3 itemIndex:indexPath.row toStatement:YES];
    }
}

- (IBAction)switchBtnFishAndSeafoodAction:(id)sender {
    
    NSIndexPath *indexPath = [self indexPathBySenderBtn:sender];
    
    //[self saveStatementForSwitchBtn:self.switchBtnFishAndSeafood by:self.fishAndSeafoodArray withKey:@"fishAndSeafoodStatements" by:indexPath];
    if (self.switchBtnFishAndSeafood.selected) {
        
        [self.switchBtnFishAndSeafood setSelected:NO];
        [self switchStatementForItemOfCatalogIndex:4 itemIndex:indexPath.row toStatement:NO];
        
    } else {
        
        [self.switchBtnFishAndSeafood setSelected:YES];
        [self switchStatementForItemOfCatalogIndex:4 itemIndex:indexPath.row toStatement:YES];
    }
}

- (IBAction)switchBtnHerbsAndSpicesAction:(id)sender {
    
    NSIndexPath *indexPath = [self indexPathBySenderBtn:sender];
    
    //[self saveStatementForSwitchBtn:self.switchBtnHerbsAndSpices by:self.herbsAndSpicesArray withKey:@"herbsAndSpicesStatements" by:indexPath];
    if (self.switchBtnHerbsAndSpices.selected) {
        
        [self.switchBtnHerbsAndSpices setSelected:NO];
        [self switchStatementForItemOfCatalogIndex:5 itemIndex:indexPath.row toStatement:NO];
        
    } else {
        
        [self.switchBtnHerbsAndSpices setSelected:YES];
        [self switchStatementForItemOfCatalogIndex:5 itemIndex:indexPath.row toStatement:YES];
    }
}

- (IBAction)switchBtnEggAndMilkProductsAction:(id)sender {
    
    NSIndexPath *indexPath = [self indexPathBySenderBtn:sender];
    
    //[self saveStatementForSwitchBtn:self.switchBtnEggAndMilkProducts by:self.eggAndMilkProductsArray withKey:@"eggAndMilkProductsStatements" by:indexPath];
    if (self.switchBtnEggAndMilkProducts.selected) {
        
        [self.switchBtnEggAndMilkProducts setSelected:NO];
        [self switchStatementForItemOfCatalogIndex:6 itemIndex:indexPath.row toStatement:NO];
        
    } else {
        
        [self.switchBtnEggAndMilkProducts setSelected:YES];
        [self switchStatementForItemOfCatalogIndex:6 itemIndex:indexPath.row toStatement:YES];
    }
}

- (IBAction)switchBtnNutsAndSeedsAction:(id)sender {
    
    NSIndexPath *indexPath = [self indexPathBySenderBtn:sender];
    
    //[self saveStatementForSwitchBtn:self.switchBtnNutsAndSeeds by:self.nutsAndSeedsArray withKey:@"nutsAndSeedsStatements" by:indexPath];
    if (self.switchBtnNutsAndSeeds.selected) {
        
        [self.switchBtnNutsAndSeeds setSelected:NO];
        [self switchStatementForItemOfCatalogIndex:7 itemIndex:indexPath.row toStatement:NO];
        
    } else {
        
        [self.switchBtnNutsAndSeeds setSelected:YES];
        [self switchStatementForItemOfCatalogIndex:7 itemIndex:indexPath.row toStatement:YES];
    }
}

- (IBAction)switchBtnOtherAction:(id)sender {
    
    NSIndexPath *indexPath = [self indexPathBySenderBtn:sender];
    
    //[self saveStatementForSwitchBtn:self.switchBtnOther by:self.otherArray withKey:@"otherStatements" by:indexPath];
    if (self.switchBtnOther.selected) {
        
        [self.switchBtnOther setSelected:NO];
        [self switchStatementForItemOfCatalogIndex:8 itemIndex:indexPath.row toStatement:NO];
        
    } else {
        
        [self.switchBtnOther setSelected:YES];
        [self switchStatementForItemOfCatalogIndex:8 itemIndex:indexPath.row toStatement:YES];
    }
}

- (IBAction)switchBtnMyAllergensAction:(id)sender {
    
    NSIndexPath *indexPath = [self indexPathBySenderBtn:sender];
    
    [self saveStatementForSwitchBtn:self.switchBtnMyAllergens by:self.myAllergensArray withKey:@"myAllergensStatements" by:indexPath];
}






- (NSIndexPath *)indexPathBySenderBtn:(id)sender {
    
    UIButton *button = (UIButton *)sender;
    
    AllergensSettingsTableViewCell *cell = (AllergensSettingsTableViewCell *)button.superview.superview;
    
    UITableView *tableView = (UITableView *)cell.superview.superview;
    
    NSIndexPath *indexPath = [tableView indexPathForCell:cell];
    return indexPath;
}

- (void)saveStatementForSwitchBtn:(UIButton *)currentSwitchBtn by:(NSMutableArray *)mArray withKey:(NSString *)key by:(NSIndexPath *)curIndexPath {
    
    if (currentSwitchBtn.selected) {
        [currentSwitchBtn setSelected:NO];
        
        mArray = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] arrayForKey:key]];
        
        [mArray replaceObjectAtIndex:curIndexPath.row withObject:[NSNumber numberWithInt:0]];
        
        [[NSUserDefaults standardUserDefaults] setObject:mArray forKey:key];
        
    } else {
        [currentSwitchBtn setSelected:YES];
        
        mArray = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] arrayForKey:key]];
        
        [mArray replaceObjectAtIndex:curIndexPath.row withObject:[NSNumber numberWithInt:1]];
        [[NSUserDefaults standardUserDefaults] setObject:mArray forKey:key];
        
    }
}




/*- (NSIndexPath *)indexPathBySender:(id)sender {
    
    UISwitch *switcher = (UISwitch *)sender;
    
    AllergensSettingsTableViewCell *cell = (AllergensSettingsTableViewCell *)switcher.superview.superview;
    
    UITableView *tableView = (UITableView *)cell.superview.superview;
    
    NSIndexPath *indexPath = [tableView indexPathForCell:cell];
    return indexPath;
}

- (void)saveStatementFor:(UISwitch *)currentSwitch by:(NSMutableArray *)mArray withKey:(NSString *)key by:(NSIndexPath *)curIndexPath {

    if (currentSwitch.on) {
        
        mArray = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] arrayForKey:key]];
        
        [mArray replaceObjectAtIndex:curIndexPath.row withObject:[NSNumber numberWithInt:1]];
        
        [[NSUserDefaults standardUserDefaults] setObject:mArray forKey:key];
        
    } else {
        
        mArray = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] arrayForKey:key]];
        
        [mArray replaceObjectAtIndex:curIndexPath.row withObject:[NSNumber numberWithInt:0]];
        [[NSUserDefaults standardUserDefaults] setObject:mArray forKey:key];
        
    }
}*/

@end
