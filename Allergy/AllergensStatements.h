//
//  AllergensStatements.h
//  Allergy
//
//  Created by Владислав on 19.01.17.
//  Copyright © 2017 Vladislav Popov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AllergensStatements : NSObject

- (NSArray*) fruitsAndBerriesStatementsArray;
- (NSArray*) vegetablesArray;
- (NSArray*) cerealsArray;
- (NSArray*) meatAndPoultryArray;
- (NSArray*) fishAndSeafoodArray;
- (NSArray*) herbsAndSpicesArray;
- (NSArray*) eggAndMilkProductsArray;
- (NSArray*) nutsAndSeedsArray;
- (NSArray*) otherArray;

@end
