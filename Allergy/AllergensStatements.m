//
//  AllergensStatements.m
//  Allergy
//
//  Created by Владислав on 19.01.17.
//  Copyright © 2017 Vladislav Popov. All rights reserved.
//

#import "AllergensStatements.h"

@implementation AllergensStatements

- (NSArray*) fruitsAndBerriesStatementsArray {
    
//    NSNumber *statement = [NSNumber numberWithBool:NO];
    
//    NSArray *fruitsAndBerries = [NSArray arrayWithObjects:
//                                 statement,
//                                 statement,
//                                 statement,
//                                 statement,
//                                 statement,
//                                 statement,
//                                 statement,
//                                 statement,
//                                 statement,
//                                 statement,
//                                 statement,
//                                 statement,
//                                 statement,
//                                 statement,
//                                 nil];//14

    NSNumber *zeroNumber = [NSNumber numberWithInt:0];
    
    NSArray *fruitsAndBerries = [NSArray arrayWithObjects:
                                 zeroNumber,
                                 zeroNumber,
                                 zeroNumber,
                                 zeroNumber,
                                 zeroNumber,
                                 zeroNumber,
                                 zeroNumber,
                                 zeroNumber,
                                 zeroNumber,
                                 zeroNumber,
                                 zeroNumber,
                                 zeroNumber,
                                 zeroNumber,
                                 zeroNumber,
                                 nil];//14

    return fruitsAndBerries;
}

- (NSArray*) vegetablesArray {
    
    NSNumber *zeroNumber = [NSNumber numberWithInt:0];
    
    NSArray *vegetables = [NSArray arrayWithObjects:
                                 zeroNumber,
                                 zeroNumber,
                                 zeroNumber,
                                 zeroNumber,
                                 zeroNumber,
                                 zeroNumber,
                                 zeroNumber,
                                 zeroNumber,
                                 zeroNumber,
                                 zeroNumber,
                                 zeroNumber,
                                 zeroNumber,
                                 zeroNumber,
                                 zeroNumber,
                                 zeroNumber,
                                 zeroNumber,
                                 zeroNumber,
                                 zeroNumber,
                                 nil];//18
    
    return vegetables;
}

- (NSArray*) cerealsArray {
    
    NSNumber *zeroNumber = [NSNumber numberWithInt:0];
    
    NSArray *cereals = [NSArray arrayWithObjects:
                           zeroNumber,
                           zeroNumber,
                           zeroNumber,
                           zeroNumber,
                           zeroNumber,
                           zeroNumber,
                           zeroNumber,
                           zeroNumber,
                           zeroNumber,
                           nil];//9
    
    return cereals;
}

- (NSArray*) meatAndPoultryArray {
    NSNumber *zeroNumber = [NSNumber numberWithInt:0];
    
    NSArray *meatAndPoultry = [NSArray arrayWithObjects:
                        zeroNumber,
                        zeroNumber,
                        zeroNumber,
                        zeroNumber,
                        zeroNumber,
                        zeroNumber,
                        nil];//6
    
    return meatAndPoultry;
}

- (NSArray*) fishAndSeafoodArray {
    NSNumber *zeroNumber = [NSNumber numberWithInt:0];
    
    NSArray *fishAndSeafood = [NSArray arrayWithObjects:
                               zeroNumber,
                               zeroNumber,
                               zeroNumber,
                               zeroNumber,
                               zeroNumber,
                               zeroNumber,
                               zeroNumber,
                               zeroNumber,
                               zeroNumber,
                               zeroNumber,
                               zeroNumber,
                               zeroNumber,
                               nil];//12
    
    return fishAndSeafood;
}

- (NSArray*) herbsAndSpicesArray {
    NSNumber *zeroNumber = [NSNumber numberWithInt:0];
    
    NSArray *herbsAndSpices = [NSArray arrayWithObjects:
                               zeroNumber,
                               zeroNumber,
                               zeroNumber,
                               zeroNumber,
                               nil];//4
    
    return herbsAndSpices;
}

- (NSArray*) eggAndMilkProductsArray {
    NSNumber *zeroNumber = [NSNumber numberWithInt:0];
    
    NSArray *eggAndMilkProducts = [NSArray arrayWithObjects:
                               zeroNumber,
                               zeroNumber,
                               zeroNumber,
                               zeroNumber,
                               zeroNumber,
                               zeroNumber,
                               zeroNumber,
                               zeroNumber,
                               zeroNumber,
                               zeroNumber,
                               zeroNumber,
                               zeroNumber,
                               nil];//12
    
    return eggAndMilkProducts;
}

- (NSArray*) nutsAndSeedsArray {
    NSNumber *zeroNumber = [NSNumber numberWithInt:0];
    
    NSArray *nutsAndSeeds = [NSArray arrayWithObjects:
                                   zeroNumber,
                                   zeroNumber,
                                   zeroNumber,
                                   zeroNumber,
                                   zeroNumber,
                                   zeroNumber,
                                   nil];//6
    
    return nutsAndSeeds;
}

- (NSArray*) otherArray {
    NSNumber *zeroNumber = [NSNumber numberWithInt:0];
    
    NSArray *other = [NSArray arrayWithObjects:
                             zeroNumber,
                             zeroNumber,
                             zeroNumber,
                             zeroNumber,
                             zeroNumber,
                             zeroNumber,
                             zeroNumber,
                             zeroNumber,
                             zeroNumber,
                             nil];//9
    
    return other;
}

@end
