//
//  CardsTableViewCell.h
//  Allergy
//
//  Created by Владислав on 10.01.17.
//  Copyright © 2017 Vladislav Popov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CardsTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *productLabel;

@end
