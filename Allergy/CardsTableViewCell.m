//
//  CardsTableViewCell.m
//  Allergy
//
//  Created by Владислав on 10.01.17.
//  Copyright © 2017 Vladislav Popov. All rights reserved.
//

#import "CardsTableViewCell.h"

@implementation CardsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
