//
//  CardsViewController.h
//  Allergy
//
//  Created by Владислав on 09.01.17.
//  Copyright © 2017 Vladislav Popov. All rights reserved.
//

#import "ViewController.h"

@interface CardsViewController : UIViewController {
    
    __weak IBOutlet UIButton *productButton;
}

-(IBAction)hideImage:(id)sender;
//-(IBAction)showImage:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *subTextLabel;

@end
