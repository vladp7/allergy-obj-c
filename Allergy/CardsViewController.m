//
//  CardsViewController.m
//  Allergy
//
//  Created by Владислав on 09.01.17.
//  Copyright © 2017 Vladislav Popov. All rights reserved.
//

#import "CardsViewController.h"
#import "Product.h"
#import "CardsTableViewCell.h"
#import "Allergens.h"
#import "Allergen.h"
#import "UserDefaults.h"

@interface CardsViewController () <UITableViewDataSource, UITableViewDelegate> {
    UserDefaults *userDefaults;
}

//@property (strong, nonatomic) UserDefaults * userDefaults;

@property (strong, nonatomic) NSArray * allergensArray;

@property (strong, nonatomic) Allergen * fruitsAndBerries;
@property (strong, nonatomic) Allergen * vegetables;
@property (strong, nonatomic) Allergen * cereals;
@property (strong, nonatomic) Allergen * meatAndPoultry;
@property (strong, nonatomic) Allergen * fishAndSeafood;
@property (strong, nonatomic) Allergen * herbsAndSpices;
@property (strong, nonatomic) Allergen * eggAndMilkProducts;
@property (strong, nonatomic) Allergen * nutsAndSeeds;
@property (strong, nonatomic) Allergen * other;

@property (strong, nonatomic) NSArray * myAllergens;


@property (strong, nonatomic) NSMutableArray *statementIsOnAllergensArray;
@property (strong, nonatomic) NSMutableArray *statementIsOnAllergensArrayImages;

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UILabel *iAmAllergicToLabel;
@property (weak, nonatomic) IBOutlet UITableView *allergensTableView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *allergensTableViewHeight;


@end

@implementation CardsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //перенос строки, если название аллергена длинное
    self.allergensTableView.rowHeight = UITableViewAutomaticDimension;
    self.allergensTableView.estimatedRowHeight = 50;
}

- (void)viewWillAppear:(BOOL)animated {
    
    self.statementIsOnAllergensArray = [[NSMutableArray alloc] init];
    self.statementIsOnAllergensArrayImages = [[NSMutableArray alloc] init];
    
    
    Allergens* allergens = [[Allergens alloc] init];
    self.allergensArray = allergens.allergensArray;
    
    
    self.fruitsAndBerries = [[Allergen alloc] init];
    self.fruitsAndBerries = [[self allergensArray] objectAtIndex:0];
    
    self.vegetables = [[Allergen alloc] init];
    self.vegetables = [[self allergensArray] objectAtIndex:1];
    
    self.cereals = [[Allergen alloc] init];
    self.cereals = [[self allergensArray] objectAtIndex:2];
    
    self.meatAndPoultry = [[Allergen alloc] init];
    self.meatAndPoultry = [[self allergensArray] objectAtIndex:3];
    
    self.fishAndSeafood = [[Allergen alloc] init];
    self.fishAndSeafood = [[self allergensArray] objectAtIndex:4];
    
    self.herbsAndSpices = [[Allergen alloc] init];
    self.herbsAndSpices = [[self allergensArray] objectAtIndex:5];
    
    self.eggAndMilkProducts = [[Allergen alloc] init];
    self.eggAndMilkProducts = [[self allergensArray] objectAtIndex:6];
    
    self.nutsAndSeeds = [[Allergen alloc] init];
    self.nutsAndSeeds = [[self allergensArray] objectAtIndex:7];
    
    self.other = [[Allergen alloc] init];
    self.other = [[self allergensArray] objectAtIndex:8];
    
    self.myAllergens = [[NSArray alloc] init];
    //self.myAllergens = [userDefaults getArrayWithParamName:@"myAllergens"];
    self.myAllergens = [[NSUserDefaults standardUserDefaults] arrayForKey:@"myAllergens"];
    
    
    /*NSArray *fruitsAndBerriesStatements = [[NSUserDefaults standardUserDefaults] arrayForKey:@"fruitsAndBerriesStatements"];
    NSArray *vegetablesStatements = [[NSUserDefaults standardUserDefaults] arrayForKey:@"vegetablesStatements"];
    NSArray *cerealsStatements = [[NSUserDefaults standardUserDefaults] arrayForKey:@"cerealsStatements"];
    NSArray *meatAndPoultryStatements = [[NSUserDefaults standardUserDefaults] arrayForKey:@"meatAndPoultryStatements"];
    NSArray *fishAndSeafoodStatements = [[NSUserDefaults standardUserDefaults] arrayForKey:@"fishAndSeafoodStatements"];
    NSArray *herbsAndSpicesStatements = [[NSUserDefaults standardUserDefaults] arrayForKey:@"herbsAndSpicesStatements"];
    NSArray *eggAndMilkProductsStatements = [[NSUserDefaults standardUserDefaults] arrayForKey:@"eggAndMilkProductsStatements"];
    NSArray *nutsAndSeedsStatements = [[NSUserDefaults standardUserDefaults] arrayForKey:@"nutsAndSeedsStatements"];
    NSArray *otherStatements = [[NSUserDefaults standardUserDefaults] arrayForKey:@"otherStatements"];*/
    NSArray *myAllergensStatements = [[NSUserDefaults standardUserDefaults] arrayForKey:@"myAllergensStatements"];

    
    
    NSDictionary *structCatalog = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"structCatalog"];
    
    //structCatalog[@"catalogs"][0][@"items"][indexPath.row][@"statement"]
    
    for (int i = 0; i < [structCatalog[@"catalogs"] count]; i++) {
        for (int j = 0; j < [structCatalog[@"catalogs"][i][@"items"] count]; j++) {
            if ([structCatalog[@"catalogs"][i][@"items"][j][@"statement"] intValue] == 1) {
                NSLog(@"statement is equal 1, title = %@", structCatalog[@"catalogs"][i][@"items"][j][@"title"]);
                NSString *title = structCatalog[@"catalogs"][i][@"items"][j][@"title"];
                NSString *fileName = structCatalog[@"catalogs"][i][@"items"][j][@"filename"];
                [self.statementIsOnAllergensArray addObject:title];
                [self.statementIsOnAllergensArrayImages addObject:fileName];
            }
        }
    }
    
    
    /*[self collectAllergensWithOnStatement:fruitsAndBerriesStatements for:self.fruitsAndBerries];
    [self collectAllergensWithOnStatement:vegetablesStatements for:self.vegetables];
    [self collectAllergensWithOnStatement:cerealsStatements for:self.cereals];
    [self collectAllergensWithOnStatement:meatAndPoultryStatements for:self.meatAndPoultry];
    [self collectAllergensWithOnStatement:fishAndSeafoodStatements for:self.fishAndSeafood];
    [self collectAllergensWithOnStatement:herbsAndSpicesStatements for:self.herbsAndSpices];
    [self collectAllergensWithOnStatement:eggAndMilkProductsStatements for:self.eggAndMilkProducts];
    [self collectAllergensWithOnStatement:nutsAndSeedsStatements for:self.nutsAndSeeds];
    [self collectAllergensWithOnStatement:otherStatements for:self.other];*/
//    [self collectAllergensWithOnStatement:myAllergensStatements for:self.myAllergens];
    
    if ([[NSUserDefaults standardUserDefaults] arrayForKey:@"myAllergens"] != nil) {
    //if ([userDefaults getArrayWithParamName:@"myAllergens"] != nil) {
    
        for (int i = 0; i < [myAllergensStatements count]; i++) {
            
            if ([[myAllergensStatements objectAtIndex:i] intValue] == 1) {
                
                NSString *str = [self.myAllergens objectAtIndex:i];
                NSString *img = @"no-allergen";
                
                [self.statementIsOnAllergensArray addObject:str];
                [self.statementIsOnAllergensArrayImages addObject:img];
            }
        }
    }
    
    if ([self.statementIsOnAllergensArray count] == 0) {
        self.containerView.hidden = YES;
        //NSLog(@"array is empty");
        self.subTextLabel.text = @"Выберите аллергены в настройках";
    } else {
        self.containerView.hidden = NO;
        
        NSMutableString *subText = [[NSMutableString alloc] init];
        
        for (int i = 0; i < [self.statementIsOnAllergensArray count]; i++) {
            subText = [subText stringByAppendingString:[self.statementIsOnAllergensArray objectAtIndex:i]];
            if (i != [self.statementIsOnAllergensArray count] - 1)
                subText = [subText stringByAppendingString: @", "];
        }
        
        [[NSUserDefaults standardUserDefaults] setObject:subText forKey:@"choosedAllergensString"];
        
        NSString *resultStr = [NSString stringWithFormat:@"%@%@", @"У меня аллергия на ", subText];
        
        self.subTextLabel.text = resultStr;
    }
    
    [self.allergensTableView reloadData];
    
    productButton.hidden = YES;
    
    

    
    
    /*
    [self.allergensTableView layoutIfNeeded];

    CGSize tableViewSize=self.allergensTableView.contentSize;
    NSLog(@"table content height = %f", tableViewSize.height);
    
    CGSize containerSize = self.containerView.frame.size;
    NSLog(@"container height = %f", containerSize.height);
    //self.containerView.
    
    CGFloat labelHeight = [self getLabelHeight:self.iAmAllergicToLabel];
    NSLog(@"label height = %f", labelHeight);
    
    if (tableViewSize.height > (containerSize.height - labelHeight)) {
        NSLog(@"11111");
        self.allergensTableViewHeight.constant = containerSize.height - labelHeight;
    } else {
        NSLog(@"22222");
        self.allergensTableViewHeight.constant = tableViewSize.height;
    }
     */


    
    //[self tableView:self.allergensTableView heightForRowAtIndexPath:indexPath];

}

- (void)viewDidAppear:(BOOL)animated {
    
    [self.allergensTableView layoutIfNeeded];
    
    CGSize tableViewSize=self.allergensTableView.contentSize;
    //NSLog(@"table content height = %f", tableViewSize.height);
    
    CGSize containerSize = self.containerView.frame.size;
    //NSLog(@"container height = %f", containerSize.height);
    
    CGFloat labelHeight = [self getLabelHeight:self.iAmAllergicToLabel];
    //NSLog(@"label height = %f", labelHeight);
    
    if (tableViewSize.height > (containerSize.height - labelHeight)) {
        self.allergensTableViewHeight.constant = containerSize.height - labelHeight;
    } else {
        self.allergensTableViewHeight.constant = tableViewSize.height;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.statementIsOnAllergensArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *SimpleIdentifier = @"productCell";
    
    CardsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:SimpleIdentifier];
    
    if (cell == nil) {
        cell = [[CardsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:SimpleIdentifier];
    }
    
    cell.productLabel.text = self.statementIsOnAllergensArray[indexPath.row];
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    productButton.hidden = NO;

    NSString* imageName = self.statementIsOnAllergensArrayImages[indexPath.row];
    NSLog(@"%@", self.statementIsOnAllergensArrayImages[indexPath.row]);
    
    [productButton setImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
//    [productButton setImage:[UIImage imageNamed:@"peanut"] forState:UIControlStateNormal];
}

#pragma mark - Actions
-(IBAction)hideImage:(id)sender {
    productButton.hidden = YES;
}

#pragma mark - Custom
/*
- (void)collectAllergensWithOnStatement:(NSArray*)array for:(Allergen*)allergen {
    NSLog(@"secondLangDict id = %@", [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"secondLangDict"][@"_id"]);
    for (int i = 0; i < [array count]; i++) {
        
        if ([[array objectAtIndex:i] intValue] == 1) {
            
            NSString *str = [[NSString alloc] init];
            
            //if ([[userDefaults getDictionaryWithParamName:@"secondLangDict"] isEqual: @"1"]) {
            //if ([[[NSUserDefaults standardUserDefaults] dictionaryForKey:@"secondLangDict"] isEqual: @"1"]) {
                str = [[allergen allergensArray] objectAtIndex:i];
            //} else {
            //    str = [[allergen allergensArrayEng] objectAtIndex:i];
            //}
            
            NSString *img = [[allergen imagesArray] objectAtIndex:i];
            
            [self.statementIsOnAllergensArray addObject:str];
            [self.statementIsOnAllergensArrayImages addObject:img];
        }
    }
    
}*/

- (CGFloat)getLabelHeight:(UILabel*)label
{
    CGSize constraint = CGSizeMake(label.frame.size.width, CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    return size.height;
}
@end
