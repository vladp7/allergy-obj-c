//
//  Contact.h
//  Allergy
//
//  Created by Владислав on 24.01.17.
//  Copyright © 2017 Vladislav Popov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Contact : NSObject

@property (strong, nonatomic) NSString* contactName;
@property (strong, nonatomic) NSString* contactPhone;

@end
