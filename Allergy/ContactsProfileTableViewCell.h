//
//  ContactsProfileTableViewCell.h
//  Allergy
//
//  Created by Владислав on 23.01.17.
//  Copyright © 2017 Vladislav Popov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactsProfileTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *contactName;
@property (weak, nonatomic) IBOutlet UILabel *contactPhone;


@end
