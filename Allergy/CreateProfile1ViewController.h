//
//  CreateProfile1ViewController.h
//  Allergy
//
//  Created by Владислав on 11.01.17.
//  Copyright © 2017 Vladislav Popov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreateProfile1ViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *interfaceLangBtn;
- (IBAction)interfaceLangBtnAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *secondLangBtn;
- (IBAction)secondLangBtnAction:(id)sender;


@property (weak, nonatomic) IBOutlet UITableView *interfaceLangTableView;
@property (weak, nonatomic) IBOutlet UITableView *secondLangTableView;

- (IBAction)onClose:(id)sender;


@property (strong, nonatomic) NSArray *langsArray;


@end
