//
//  CreateProfile1ViewController.m
//  Allergy
//
//  Created by Владислав on 11.01.17.
//  Copyright © 2017 Vladislav Popov. All rights reserved.
//

#import "CreateProfile1ViewController.h"
#import "Languages.h"

@interface CreateProfile1ViewController ()

@property (strong, nonatomic) NSString* interfaceLang;
@property (strong, nonatomic) NSString* secondLang;

@property (weak, nonatomic) IBOutlet UIImageView *interfaceLangImg;
@property (weak, nonatomic) IBOutlet UIImageView *secondLangImg;


@end

@implementation CreateProfile1ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    Languages* langs = [[Languages alloc] init];
    self.langsArray = langs.languagesArray;
    
    self.interfaceLangTableView.delegate = self;
    self.interfaceLangTableView.dataSource = self;
    
    self.secondLangTableView.delegate = self;
    self.secondLangTableView.dataSource = self;
    
    self.interfaceLangTableView.hidden = YES;
    self.secondLangTableView.hidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.langsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *simpleIdentifier = @"mutualCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleIdentifier];
    }
    
    cell.textLabel.text = [self.langsArray objectAtIndex:indexPath.row];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView == self.interfaceLangTableView) {
        
        UITableViewCell *cell = [self.interfaceLangTableView cellForRowAtIndexPath:indexPath];
        [self.interfaceLangBtn setTitle:cell.textLabel.text forState:UIControlStateNormal];
        
        switch (indexPath.row) {
            case 0:
//                [[NSUserDefaults standardUserDefaults] setObject:@"english" forKey:@"interfaceLang"];
                self.interfaceLang = @"english";
                break;
            case 1:
//                [[NSUserDefaults standardUserDefaults] setObject:@"russian" forKey:@"interfaceLang"];
                self.interfaceLang = @"russian";
                break;
                
            default:
                break;
        }
        
        self.interfaceLangTableView.hidden = YES;
        
    } else if (tableView == self.secondLangTableView) {
        
        UITableViewCell *cell = [self.secondLangTableView cellForRowAtIndexPath:indexPath];
        [self.secondLangBtn setTitle:cell.textLabel.text forState:UIControlStateNormal];
        
        switch (indexPath.row) {
            case 0:
//                [[NSUserDefaults standardUserDefaults] setObject:@"english" forKey:@"secondLang"];
                self.secondLang = @"english";
                break;
            case 1:
//                [[NSUserDefaults standardUserDefaults] setObject:@"russian" forKey:@"secondLang"];
                self.secondLang = @"russian";
                break;
                
            default:
                break;
        }
        
        self.secondLangTableView.hidden = YES;
    }
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"presentModCP2"]) {
        
        [[NSUserDefaults standardUserDefaults] setObject:self.interfaceLang forKey:@"interfaceLang"];
        [[NSUserDefaults standardUserDefaults] setObject:self.secondLang forKey:@"secondLang"];
    }
}

- (IBAction)interfaceLangBtnAction:(id)sender {
    if (self.interfaceLangTableView.hidden == YES) {
        self.interfaceLangTableView.hidden = NO;
        self.interfaceLangImg.image = [UIImage imageNamed: @"Icon - Down"];
    } else {
        self.interfaceLangTableView.hidden = YES;
        self.interfaceLangImg.image = [UIImage imageNamed: @"Icon - Right"];
    }
}

- (IBAction)secondLangBtnAction:(id)sender {
    if (self.secondLangTableView.hidden == YES) {
        self.secondLangTableView.hidden = NO;
        self.interfaceLangImg.image = [UIImage imageNamed: @"Icon - Down"];
    } else {
        self.secondLangTableView.hidden = YES;
        self.interfaceLangImg.image = [UIImage imageNamed: @"Icon - Right"];
    }
}

- (IBAction)onClose:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
