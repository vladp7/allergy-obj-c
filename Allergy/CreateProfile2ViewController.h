//
//  CreateProfile2ViewController.h
//  Allergy
//
//  Created by Владислав on 11.01.17.
//  Copyright © 2017 Vladislav Popov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreateProfile2ViewController : UIViewController <UITextFieldDelegate> {
    UIDatePicker *datePicker;
}
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;

@property (weak, nonatomic) IBOutlet UITextField *dateSelectionTextField;

- (IBAction)onClose:(id)sender;

@end
