//
//  CreateProfile2ViewController.m
//  Allergy
//
//  Created by Владислав on 11.01.17.
//  Copyright © 2017 Vladislav Popov. All rights reserved.
//

#import "CreateProfile2ViewController.h"

@interface CreateProfile2ViewController ()

@end

@implementation CreateProfile2ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //stackoverflow
    
//    self.dateSelectionTextField.delegate = self;
//    
//    // alloc/init your date picker, and (optional) set its initial date
//    datePicker = [[UIDatePicker alloc]init];
//    [datePicker setDate:[NSDate date]]; //this returns today's date
//    
//    // theMinimumDate (which signifies the oldest a person can be) and theMaximumDate (defines the youngest a person can be) are the dates you need to define according to your requirements, declare them:
//    
//    // the date string for the minimum age required (change according to your needs)
//    NSString *maxDateString = @"01-Jan-1996";
//    // the date formatter used to convert string to date
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    // the specific format to use
//    dateFormatter.dateFormat = @"dd-MMM-yyyy";
//    // converting string to date
//    NSDate *theMaximumDate = [dateFormatter dateFromString: maxDateString];
//    
//    // repeat the same logic for theMinimumDate if needed
//    
//    // here you can assign the max and min dates to your datePicker
//    [datePicker setMaximumDate:theMaximumDate]; //the min age restriction
////    [datePicker setMinimumDate:theMinimumDate]; //the max age restriction (if needed, or else dont use this line)]
//    
//    // set the mode
//    [datePicker setDatePickerMode:UIDatePickerModeDate];
//    
//    // update the textfield with the date everytime it changes with selector defined below
//    [datePicker addTarget:self action:@selector(updateTextField:) forControlEvents:UIControlEventValueChanged];
//    
//    // and finally set the datePicker as the input mode of your textfield
//    [self.dateSelectionTextField setInputView:datePicker];
    
    //youtube:
    
    datePicker = [[UIDatePicker alloc] init];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [self.dateSelectionTextField setInputView:datePicker];
    
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    [toolBar setTintColor:[UIColor grayColor]];
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(ShowSelectedDate)];
    UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar setItems:[NSArray arrayWithObjects:space, doneBtn, nil]];
    [self.dateSelectionTextField setInputAccessoryView:toolBar];
}

//youtube:

- (void)ShowSelectedDate {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/mm/yyyy"];
//    [formatter setDateFormat:@"dd/MMM/YYYY hh:min a"];
    
    NSString *dateStr = [NSString stringWithFormat:@"%@", [formatter stringFromDate:datePicker.date]];
    
    self.dateSelectionTextField.text = dateStr;
    
//    [[NSUserDefaults standardUserDefaults] setObject:dateStr forKey:@"birthday"];
    
    [self.dateSelectionTextField resignFirstResponder];
}

//stackoverflow:

//-(void)updateTextField:(id)sender {
//    UIDatePicker *picker = (UIDatePicker*)self.dateSelectionTextField.inputView;
//    self.dateSelectionTextField.text = [self formatDate:picker.date];
//}
//
//- (NSString *)formatDate:(NSDate *)date {
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
//    [dateFormatter setDateFormat:@"dd-MMM-yyyy"];
//    NSString *formattedDate = [dateFormatter stringFromDate:date];
//    return formattedDate;
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender  {
    if ([[segue identifier] isEqualToString:@"presentModCP3"]) {
        
        if (self.nameTextField.text.length > 0) {
            [[NSUserDefaults standardUserDefaults] setObject:self.nameTextField.text forKey:@"name"];
        }
        
        if (self.dateSelectionTextField.text.length > 0) {
            [[NSUserDefaults standardUserDefaults] setObject:self.dateSelectionTextField.text forKey:@"birthday"];
        }
    }
}

#pragma  mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
//    NSString *nameText = textField.text;
    
    [self.nameTextField resignFirstResponder];
    
//    NSLog(@"%@", textField.text);
    
//    [[NSUserDefaults standardUserDefaults] setObject:nameText forKey:@"name"];
    
    return YES;
}

#pragma mark actions
- (IBAction)onClose:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*- (IBAction)backgroundTapped:(id)sender {
    [self.view endEditing:YES];
}*/


@end
