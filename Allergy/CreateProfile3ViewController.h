//
//  CreateProfile3ViewController.h
//  Allergy
//
//  Created by Владислав on 11.01.17.
//  Copyright © 2017 Vladislav Popov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreateProfile3ViewController : UIViewController <UITextFieldDelegate> {
    CGFloat keyboardAnimatedDistance;
}

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;

@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;

//@property (weak, nonatomic) IBOutlet UITextField *contactNameTextField;

//@property (weak, nonatomic) IBOutlet UITextField *contactPhoneTextField;

- (IBAction)onClose:(id)sender;

@end
