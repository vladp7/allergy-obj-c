//
//  CreateProfile3ViewController.m
//  Allergy
//
//  Created by Владислав on 11.01.17.
//  Copyright © 2017 Vladislav Popov. All rights reserved.
//

#import "CreateProfile3ViewController.h"
#import "AllergensStatements.h"

@interface CreateProfile3ViewController ()

@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;

//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerHeight;

@end

@implementation CreateProfile3ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self phoneToolbar];
    //[self phoneContactToolbar];

    [self registerForKeyboardNotifications];
}

- (void)viewWillAppear:(BOOL)animated {
    
    //if ([[UIScreen mainScreen] bounds].size.height < 667) {
        //self.containerHeight.constant = 367;
    //}
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([[segue identifier] isEqualToString:@"showCardsVC"]) {
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isLogged"];
        
        [[NSUserDefaults standardUserDefaults] setObject:self.emailTextField.text forKey:@"email"];
        
        [[NSUserDefaults standardUserDefaults] setObject:self.phoneTextField.text forKey:@"phone"];
        
        /*
        NSString *defaultContactName = @"No name";
        NSString *defaultContactPhone = @"No phone";
        
        
        
        if (self.contactNameTextField.text.length == 0) {
            if (self.contactPhoneTextField.text.length == 0) {
                //NSDictionary* contact = @{@"contactName":defaultContactName, @"contactPhone":defaultContactPhone};
                //NSArray *contacts = @[contact];
                //[[NSUserDefaults standardUserDefaults] setObject:contacts forKey:@"contacts"];
                [self saveContactWithName:defaultContactName andPhone:defaultContactPhone];
                
            } else {
                //NSDictionary* contact = @{@"contactName":defaultContactName, @"contactPhone":self.contactPhoneTextField.text};
                //NSArray *contacts = @[contact];
                //[[NSUserDefaults standardUserDefaults] setObject:contacts forKey:@"contacts"];
                [self saveContactWithName:defaultContactName andPhone:self.contactPhoneTextField.text];
                
            }
        } else if (self.contactPhoneTextField.text.length == 0) {
            //NSDictionary* contact = @{@"contactName":self.contactNameTextField.text, @"contactPhone":defaultContactPhone};
            //NSArray *contacts = @[contact];
            //[[NSUserDefaults standardUserDefaults] setObject:contacts forKey:@"contacts"];
            [self saveContactWithName:self.contactNameTextField.text andPhone:defaultContactPhone];
        } else {
            //NSDictionary* contact = @{@"contactName":self.contactNameTextField.text, @"contactPhone":self.contactPhoneTextField.text};
            //NSArray *contacts = @[contact];
            //[[NSUserDefaults standardUserDefaults] setObject:contacts forKey:@"contacts"];
            [self saveContactWithName:self.contactNameTextField.text andPhone:self.contactPhoneTextField.text];
        }
        
        
        NSDictionary* contact = @{@"contactName":@"Petrov", @"contactPhone":@"Vasiliy"};
        
        NSArray* array = @[@"String 1", @"String 2", @"String 3"];
        
        */
        
        AllergensStatements *allergensStatements = [[AllergensStatements alloc] init];
        
        [[NSUserDefaults standardUserDefaults] setObject:allergensStatements.fruitsAndBerriesStatementsArray forKey:@"fruitsAndBerriesStatements"];
        [[NSUserDefaults standardUserDefaults] setObject:allergensStatements.vegetablesArray forKey:@"vegetablesStatements"];
        [[NSUserDefaults standardUserDefaults] setObject:allergensStatements.cerealsArray forKey:@"cerealsStatements"];
        [[NSUserDefaults standardUserDefaults] setObject:allergensStatements.meatAndPoultryArray forKey:@"meatAndPoultryStatements"];
        [[NSUserDefaults standardUserDefaults] setObject:allergensStatements.fishAndSeafoodArray forKey:@"fishAndSeafoodStatements"];
        [[NSUserDefaults standardUserDefaults] setObject:allergensStatements.herbsAndSpicesArray forKey:@"herbsAndSpicesStatements"];
        [[NSUserDefaults standardUserDefaults] setObject:allergensStatements.eggAndMilkProductsArray forKey:@"eggAndMilkProductsStatements"];
        [[NSUserDefaults standardUserDefaults] setObject:allergensStatements.nutsAndSeedsArray forKey:@"nutsAndSeedsStatements"];
        [[NSUserDefaults standardUserDefaults] setObject:allergensStatements.otherArray forKey:@"otherStatements"];
    }
}

#pragma  mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (textField == self.emailTextField) {
        
//        [[NSUserDefaults standardUserDefaults] setObject:textField.text forKey:@"email"];
        
        [self.emailTextField resignFirstResponder];
        
    } else if (textField == self.phoneTextField) {
        
//        [self.phoneTextField resignFirstResponder];
        
    //} else if (textField == self.contactNameTextField) {
        
//        [[NSUserDefaults standardUserDefaults] setObject:textField.text forKey:@"contactName"];
        
        //[self.contactNameTextField resignFirstResponder];
        
    //} else if (textField == self.contactPhoneTextField) {
        
//        [self.contactPhoneTextField resignFirstResponder];
        
    }
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    //[self animateTextField:textField up:YES];
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    //[self animateTextField:textField up:NO];
}

#pragma mark - Custom methods
/*- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    int animatedDistance;
    int moveUpValue = textField.frame.origin.y+ textField.frame.size.height;
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        
//        animatedDistance = 216-(460-moveUpValue-5);
        animatedDistance = 287-(460-moveUpValue-5);
    }
    else
    {
//        animatedDistance = 162-(320-moveUpValue-5);
        animatedDistance = 233-(320-moveUpValue-5);
    }
    
    if(animatedDistance>0)
    {
        const int movementDistance = animatedDistance;
        const float movementDuration = 0.3f;
        int movement = (up ? -movementDistance : movementDistance);
        [UIView beginAnimations: nil context: nil];
        [UIView setAnimationBeginsFromCurrentState: YES];
        [UIView setAnimationDuration: movementDuration];
        self.view.frame = CGRectOffset(self.view.frame, 0, movement);
        [UIView commitAnimations];
    }
}*/



//-(void)cancelNumberPad{
//    [self.phoneTextField resignFirstResponder];
//    self.phoneTextField.text = @"";
//}

- (void)phoneToolbar {
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    [numberToolbar setTintColor:[UIColor grayColor]];
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)];
    UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [numberToolbar setItems:[NSArray arrayWithObjects:space, doneBtn, nil]];
    [numberToolbar sizeToFit];
    self.phoneTextField.inputAccessoryView = numberToolbar;
    
}

/*
- (void)phoneContactToolbar {
    UIToolbar* numberContactToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    [numberContactToolbar setTintColor:[UIColor grayColor]];
    UIBarButtonItem *doneContactBtn = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneContactWithNumberPad)];
    UIBarButtonItem *spaceContact = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [numberContactToolbar setItems:[NSArray arrayWithObjects:spaceContact, doneContactBtn, nil]];
    [numberContactToolbar sizeToFit];
    self.contactPhoneTextField.inputAccessoryView = numberContactToolbar;
    
}*/

-(void)doneWithNumberPad{
//    NSString *numberFromTheKeyboard = self.phoneTextField.text;
    
//    [[NSUserDefaults standardUserDefaults] setObject:numberFromTheKeyboard forKey:@"phone"];
    
    [self.phoneTextField resignFirstResponder];
}

/*
-(void)doneContactWithNumberPad{
//    NSString *numberFromTheKeyboard = self.contactPhoneTextField.text;
    
//    [[NSUserDefaults standardUserDefaults] setObject:numberFromTheKeyboard forKey:@"contactPhone"];
    
    [self.contactPhoneTextField resignFirstResponder];
}*/

#pragma mark - Actions
- (IBAction)onClose:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Custom methods
/*- (void)saveContactWithName:(NSString*)name andPhone:(NSString*)phone {
    
    NSDictionary* contact = @{@"contactName":name, @"contactPhone":phone};
    NSArray *contacts = @[contact];
    [[NSUserDefaults standardUserDefaults] setObject:contacts forKey:@"contacts"];
}*/





- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.mainScrollView.contentInset = contentInsets;
    self.mainScrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, self.emailTextField.frame.origin) ) {
        CGPoint scrollPoint = CGPointMake(0.0, self.emailTextField.frame.origin.y-kbSize.height);
        [self.mainScrollView setContentOffset:scrollPoint animated:YES];
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.mainScrollView.contentInset = contentInsets;
    self.mainScrollView.scrollIndicatorInsets = contentInsets;
}



@end
