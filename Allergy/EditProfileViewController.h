//
//  EditProfileViewController.h
//  Allergy
//
//  Created by Владислав on 15.01.17.
//  Copyright © 2017 Vladislav Popov. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "ProfileViewController.h"


@interface EditProfileViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *birthdayTextField;

@property (weak, nonatomic) IBOutlet UIButton *saveBtn;

- (IBAction)onClose:(id)sender;

@end

