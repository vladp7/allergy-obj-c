//
//  EditProfileViewController.m
//  Allergy
//
//  Created by Владислав on 15.01.17.
//  Copyright © 2017 Vladislav Popov. All rights reserved.
//

#import "EditProfileViewController.h"
#import "ContactsTableViewCell.h"
#import <ContactsUI/ContactsUI.h>
#import "ServerManager.h"

@interface EditProfileViewController () <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, CNContactPickerDelegate> {
    UIDatePicker *datePicker;
    //NSUInteger contactId;
}

@property (assign, nonatomic) NSInteger contactId;

@property (weak, nonatomic) IBOutlet UITableView *contactsTableView;

@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;

@property (weak, nonatomic) IBOutlet UILabel *choosedAllergensLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contactsTableViewHeight;

@property (strong, nonatomic) NSArray* contactsArray;

@end

@implementation EditProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.nameTextField.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"name"];
    self.birthdayTextField.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"birthday"];
    
    datePicker = [[UIDatePicker alloc] init];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [self.birthdayTextField setInputView:datePicker];
    
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    [toolBar setTintColor:[UIColor grayColor]];
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(ShowSelectedDate)];
    UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar setItems:[NSArray arrayWithObjects:space, doneBtn, nil]];
    [self.birthdayTextField setInputAccessoryView:toolBar];
    
    if ([[NSUserDefaults standardUserDefaults] stringForKey:@"choosedAllergensString"] != nil) {
        NSString *choosedAllergens = [[NSUserDefaults standardUserDefaults] stringForKey:@"choosedAllergensString"];
        self.choosedAllergensLabel.text = choosedAllergens;
    }
    
    [self.contactsTableView setEditing:YES animated:YES];
    
    [self registerForKeyboardNotifications];
}

- (void)viewWillAppear:(BOOL)animated {
    
    self.contactsArray = [[NSArray alloc] init];
    self.contactsArray =  [[NSUserDefaults standardUserDefaults] arrayForKey:@"contacts"];
    self.contactsTableViewHeight.constant = ([self.contactsArray count] + 1) * self.contactsTableView.rowHeight ;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma  mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.contactsArray count] + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *simpleIdentifier = @"contactCell";
    
    ContactsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleIdentifier];
    
    if (cell == nil) {
        cell = [[ContactsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleIdentifier];
    }
    
    if (indexPath.row == [self.contactsArray count]) {
        cell.addContact.hidden = NO;
        cell.contactName.hidden = YES;
        cell.contactPhone.hidden = YES;
        
    } else {
        NSString *contactName = [[[[NSUserDefaults standardUserDefaults] arrayForKey:@"contacts"] objectAtIndex:indexPath.row] objectForKey:@"contactName"];
        
        NSString *contactPhone = [[[[NSUserDefaults standardUserDefaults] arrayForKey:@"contacts"] objectAtIndex:indexPath.row] objectForKey:@"contactPhone"];
        
        cell.addContact.hidden = YES;
        cell.contactName.hidden = NO;
        cell.contactPhone.hidden = NO;
        cell.contactName.text = contactName;
        cell.contactPhone.text = contactPhone;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        NSMutableArray *tempAr = [NSMutableArray arrayWithArray: [[NSUserDefaults standardUserDefaults] arrayForKey:@"contacts"]];
        
        NSLog(@"tempAr contact id = %@", tempAr[indexPath.row][@"contactId"]);
        
        [self deleteContactId:[tempAr[indexPath.row][@"contactId"] intValue]];
        [tempAr removeObjectAtIndex:indexPath.row];
        
        self.contactsArray = tempAr;
        [[NSUserDefaults standardUserDefaults] setObject:tempAr forKey:@"contacts"];
        
        [self.contactsTableView deleteRowsAtIndexPaths:[NSMutableArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
        //        [self.allergensScrollView setContentOffset:CGPointMake(0, self.allergensScrollView.contentOffset.y - self.myAllergensTableView.rowHeight) animated:YES];
        
        //self.myAllergensBottomConstraint.constant = self.myAllergensBottomConstraint.constant - self.myAllergensTableView.rowHeight;
        //self.myAllergensTableViewHeightConstraint.constant = self.myAllergensTableViewHeightConstraint.constant - self.myAllergensTableView.rowHeight;
        
        self.contactsTableViewHeight.constant -= self.contactsTableView.rowHeight;
        
        

        
    } else {
        
        NSLog(@"+ pressed");
        
        //[self newRowAlert];
        
        
        
        CNContactPickerViewController *picker = [[CNContactPickerViewController alloc] init];
        picker.delegate = self;
        picker.displayedPropertyKeys = @[CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey];
        [self presentViewController:picker animated:YES completion:nil];

        //[self.contactsTableView reloadData];
        //[self.view setNeedsDisplay];
        
        
        /*
        NSDictionary* contact = @{@"contactName":@"Тестовое имя", @"contactPhone":@"88005551122"};
        
        NSMutableArray *tempAr = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"contacts"]];
        
        [tempAr addObject:contact];
        
        self.contactsArray = tempAr;
        [[NSUserDefaults standardUserDefaults] setObject:tempAr forKey:@"contacts"];
        
        
        NSIndexPath *path = [NSIndexPath indexPathForRow:[self.contactsArray count] inSection:0];
        NSArray *indexArray = [NSArray arrayWithObjects:path,nil];
        
        [self.contactsTableView insertRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationRight];
        
        self.contactsTableViewHeight.constant += self.contactsTableView.rowHeight;
         */
        
        //[self.contactsTableView reloadData];
        //[self.contactsTableView reloadRowsAtIndexPaths:[NSMutableArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
}

#pragma mark - UITableViewDelegate
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == [self.contactsArray count]) {
        return UITableViewCellEditingStyleInsert;
    } else {
        return UITableViewCellEditingStyleDelete;
    }
    
}

#pragma  mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (textField == self.nameTextField) {
        
        [self.nameTextField resignFirstResponder];
        
    } else if (textField == self.birthdayTextField) {
        
        [self.birthdayTextField resignFirstResponder];
        
    }
    
    return YES;
}

/*
- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField == self.nameTextField) {
        
        [self setObject:self.nameTextField.text withParamName:@"name"];
        
    } else if (textField == self.birthdayTextField) {
        
        [self setObject:self.birthdayTextField.text withParamName:@"birthday"];
        
    }
    
}*/

#pragma mark - CNContactPickerDelegate
- (void) contactPicker:(CNContactPickerViewController *)picker didSelectContact:(CNContact *)contact{
    
    NSString *name = [[contact.givenName stringByAppendingString:@" "] stringByAppendingString:contact.familyName];
    NSLog(@"Contact : %@ %@",contact.givenName, contact.familyName);
    
    //CNContact *curContact = contact;
    
    NSArray <CNLabeledValue<CNPhoneNumber *> *> *phoneNumbers = contact.phoneNumbers;
    CNLabeledValue<CNPhoneNumber *> *firstPhone = [phoneNumbers firstObject];
    CNPhoneNumber *number = firstPhone.value;
    NSString *digits = number.stringValue;
    
    NSUInteger index = [self.contactsArray count];
//    NSNumber* contactId = [self addContactName:name contactPhone:digits ];
    __block NSNumber *contactId = [[NSNumber alloc] init];
    [self addContactName:name contactPhone:digits onSuccess:^(NSNumber *contId) {
        self.contactId = [contId intValue];
        NSLog(@"contactId inside = %d", self.contactId);
        //помещено внуть блока, т.к. возникают проблемы при асинхронности
        NSMutableArray *tempAr = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"contacts"]];
        NSDictionary* addingContact = @{@"index":@(index), @"contactId":@(self.contactId), @"contactName":name, @"contactPhone":digits};
        [tempAr addObject:addingContact];
        NSLog(@"tempAr = %@", tempAr);
        self.contactsArray = tempAr;
        [[NSUserDefaults standardUserDefaults] setObject:tempAr forKey:@"contacts"];
        NSIndexPath *path = [NSIndexPath indexPathForRow:[self.contactsArray count] inSection:0];
        NSArray *indexArray = [NSArray arrayWithObjects:path,nil];
        [self.contactsTableView insertRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationRight];
        self.contactsTableViewHeight.constant += self.contactsTableView.rowHeight;
        [self.contactsTableView reloadData];
    }];
//    [self addContactName:name contactPhone:digits];
    //NSLog(@"contactId outside = %d", self.contactId);
    
    //NSMutableArray *tempAr = [NSMutableArray arrayWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"contacts"]];
    
    //NSString *contactId = [@([tempAr count] + 1) stringValue];
    
    //NSLog(@"phone number: %@, contact Id = %@", digits, contactId);
    
    //NSDictionary* addingContact = @{@"id":contactId, @"contactName":name, @"contactPhone":digits};
    //NSDictionary* addingContact = @{@"index":@(index), @"contactId":contactId, @"contactName":name, @"contactPhone":digits};
    //NSDictionary* addingContact = @{@"index":@(index), @"contactName":name, @"contactPhone":digits};
    
    //[tempAr addObject:addingContact];
    
    //self.contactsArray = tempAr;
    //[[NSUserDefaults standardUserDefaults] setObject:tempAr forKey:@"contacts"];
    
    
    //NSIndexPath *path = [NSIndexPath indexPathForRow:[self.contactsArray count] inSection:0];
    //NSArray *indexArray = [NSArray arrayWithObjects:path,nil];
    
    /*
    [CATransaction begin];
    [self.contactsTableView beginUpdates];
    
    [CATransaction setCompletionBlock: ^{
        [self.contactsTableView reloadData];
    }];*/

    //[self.contactsTableView insertRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationRight];
    /*
    [self.contactsTableView endUpdates];
    [CATransaction commit];*/
    
    //self.contactsTableViewHeight.constant += self.contactsTableView.rowHeight;
    
    //[self.contactsTableView reloadData];
    //[self.view setNeedsDisplay];
}

- (void)contactPickerDidCancel:(CNContactPickerViewController *)picker {
    NSLog(@"Cancelled");
}

#pragma mark - API
- (void)updateProfile {
    
    NSString *name;
    NSString *email;
    NSString *phone;
    NSString *bdate;
    NSUInteger languageId;
    
    if ([self getObjectWithParamName:@"name"] != nil) {
        name = [self getObjectWithParamName:@"name"];
    } else {
        name = @"";
    }
    if ([self getObjectWithParamName:@"email"] != nil) {
        email = [self getObjectWithParamName:@"email"];
    } else {
        email = @"";
    }
    if ([self getObjectWithParamName:@"phone"] != nil) {
        phone = [self getObjectWithParamName:@"phone"];
    } else {
        phone = @"";
    }
    if ([self getObjectWithParamName:@"birthday"] != nil) {
        bdate = [self getObjectWithParamName:@"birthday"];
    } else {
        bdate = @"";
    }
    if ([self getObjectWithParamName:@"secondLangDict"] != nil) {
        languageId = (NSUInteger)[[self getObjectWithParamName:@"secondLangDict"][@"_id"] integerValue];
    } else {
        languageId = 1;
    }
    
    [[ServerManager sharedManager] updateProfileForName:name
                                                  email:email
                                                  phone:phone
                                                  bdate:bdate
                                               language:languageId
                                              onSuccess:^(id response) {
                                                  
                                                  NSString *string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
                                                  NSLog(@"update profile response = %@", string);
                                                  
                                              } onFailure:^(NSError *error, NSInteger statusCode) {
                                                  NSLog(@"error = %@, code = %d", [error localizedDescription], statusCode);
                                              }
     ];
}

- (void)addContactName:(NSString*)contactName
          contactPhone:(NSString*)contactPhone
            onSuccess:(void(^)(NSNumber* contactId)) success {
    
   // __block NSNumber *contId = [[NSNumber alloc] init];
    
    [[ServerManager sharedManager] addContactName:contactName
                                    contactPhone:contactPhone
                                       onSuccess:^(NSNumber* contactId) {
                                           //NSString *string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
                                           NSLog(@"add contact response contactId = %@", contactId);
                                           if (success) {
                                               success(contactId);
                                           }
                                           //contId = contactId;
                                       }
                                       onFailure:^(NSError *error, NSInteger statusCode) {
                                           NSLog(@"error = %@, code = %d", [error localizedDescription], statusCode);
                                       }
     ];
    //return contId;
}

- (void)deleteContactId:(NSUInteger) contactId {

    [[ServerManager sharedManager] deleteContactId:contactId
                                         onSuccess:^(id response) {
                                             NSString *string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
                                             NSLog(@"delete contact response = %@", string);
                                         }
                                         onFailure:^(NSError *error, NSInteger statusCode) {
                                             NSLog(@"error = %@, code = %d", [error localizedDescription], statusCode);
                                         }
     ];
}

#pragma mark - NSUserDefaults
- (void)setObject:(id)object withParamName:(NSString*)paramName
{
    if (object == nil)
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:paramName];
    else
        [[NSUserDefaults standardUserDefaults] setObject:object forKey:paramName];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (id)getObjectWithParamName:(NSString*)paramName
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:paramName];
}

#pragma mark - Custom methods
/*-(void)newRowAlert {
    NSString *title = @"Добавить новый контакт";
    NSString *message = @"Введите имя и телефон";
    NSString *okText = @"OK";
    NSString *cancelText = @"Cancel";
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okButton = [UIAlertAction actionWithTitle:okText style:UIAlertActionStyleDefault handler:nil];
    UIAlertAction *cancelButton = [UIAlertAction actionWithTitle:cancelText style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:okButton];
    [alert addAction:cancelButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}*/

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.mainScrollView.contentInset = contentInsets;
    self.mainScrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your application might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    if (!CGRectContainsPoint(aRect, self.birthdayTextField.frame.origin) ) {
        CGPoint scrollPoint = CGPointMake(0.0, self.birthdayTextField.frame.origin.y-kbSize.height);
        [self.mainScrollView setContentOffset:scrollPoint animated:YES];
    }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.mainScrollView.contentInset = contentInsets;
    self.mainScrollView.scrollIndicatorInsets = contentInsets;
}

- (void)ShowSelectedDate {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy/MM/dd"];
    //[formatter setDateFormat:@"dd/mm/yyyy"];
    //    [formatter setDateFormat:@"dd/MMM/YYYY hh:min a"];
    
    NSString *dateStr = [NSString stringWithFormat:@"%@", [formatter stringFromDate:datePicker.date]];
    
    self.birthdayTextField.text = dateStr;
    
    //[self setObject:dateStr withParamName:@"birthday"];
    
    [self.birthdayTextField resignFirstResponder];
}

#pragma mark - Actions
- (IBAction)onClose:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)saveBtnAction:(id)sender {
    
    [[NSUserDefaults standardUserDefaults] setObject:self.nameTextField.text forKey:@"name"];
    [[NSUserDefaults standardUserDefaults] setObject:self.birthdayTextField.text forKey:@"birthday"];
    
    [self updateProfile];
    
    //    ProfileViewController *vc= [[ProfileViewController alloc] init];
    
    //    [_delegate dataUpdated:[NSArray arrayWithObject:@"Object Passed"]];
    
    [self dismissViewControllerAnimated:NO completion:nil];
}

@end
