//
//  FileManager.h
//  Allergy
//
//  Created by Владислав on 02.02.17.
//  Copyright © 2017 Vladislav Popov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FileManager : NSObject

- (void)writeStringToFile:(NSString*)aString withName:(NSString*)fileName;
- (NSString*)readStringFromFile:(NSString *)fileName;
- (void)deleteFile:(NSString*)fileName;

@end
