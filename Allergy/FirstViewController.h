//
//  FirstViewController.h
//  Allergy
//
//  Created by Владислав on 01.02.17.
//  Copyright © 2017 Vladislav Popov. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Reachability.h"
#import <SystemConfiguration/SystemConfiguration.h>

@interface FirstViewController : UIViewController

- (BOOL)connected;

@end
