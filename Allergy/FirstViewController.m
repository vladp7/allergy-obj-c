//
//  FirstViewController.m
//  Allergy
//
//  Created by Владислав on 01.02.17.
//  Copyright © 2017 Vladislav Popov. All rights reserved.
//

#import "FirstViewController.h"

#import "ServerManager.h"
#import "FileManager.h"
#import "UserDefaults.h"

@interface FirstViewController () {
    //UserDefaults *userDefaults;
}

@end

@implementation FirstViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

-(void)viewDidAppear:(BOOL)animated {
    
    if (![self connected]) {
        [self notConnectedAlertMessage];
    }
    
    //if ([userDefaults getObjectWithParamName:@"user_token"] == nil) {
    if ([self getObjectWithParamName:@"user_token"] == nil) {

        [self newSession];
     
        NSString* filePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];

        NSString* catalogFileAtPath = [filePath stringByAppendingPathComponent:@"catalog.json"];
        if (![[NSFileManager defaultManager] fileExistsAtPath:catalogFileAtPath]) {
            [self catalog];
        }
        
        NSString* structCatalogFileAtPath = [filePath stringByAppendingPathComponent:@"structCatalog.json"];
        if (![[NSFileManager defaultManager] fileExistsAtPath:structCatalogFileAtPath]) {
            [self structCatalog];
        }
        
        //[self expandCatalogDict];
        //добавить всем аллергенам statement = NO
        [self expandStructCatalogDict];
        
        NSString* languagesFileAtPath = [filePath stringByAppendingPathComponent:@"languages.json"];
        if (![[NSFileManager defaultManager] fileExistsAtPath:languagesFileAtPath]) {
            [self languages];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - API
- (void)newSession {
    [[ServerManager sharedManager] newSessionOnSuccess:^(NSString *session) {
//        [userDefaults setObject:session withParamName:@"user_token"];
        [self setObject:session withParamName:@"user_token"];

    }
                                             onFailure:^(NSError *error, NSInteger statusCode) {
                                                 NSLog(@"error = %@, code = %d", [error localizedDescription], statusCode);
                                             }
     ];
}

- (void)catalog {
    [[ServerManager sharedManager] catalogOnSuccess:^(NSString *response) {
        
        FileManager *fileManager = [[FileManager alloc] init];
        
        [fileManager writeStringToFile:response withName:@"catalog.json"];
        
        //NSLog(@"catalog response = %@", response);
        
        }
                                          onFailure:^(NSError *error, NSInteger statusCode) {
                                                 NSLog(@"error = %@, code = %d", [error localizedDescription], statusCode);
                                          }
     ];
}


- (void)structCatalog {
    [[ServerManager sharedManager] structCatalogOnSuccess:^(NSString *response) {
        
        FileManager *fileManager = [[FileManager alloc] init];
        
        [fileManager writeStringToFile:response withName:@"structCatalog.json"];
        
        NSLog(@"struct catalog response = %@", response);
        
    }
                                          onFailure:^(NSError *error, NSInteger statusCode) {
                                              NSLog(@"error = %@, code = %d", [error localizedDescription], statusCode);
                                          }
     ];
}

- (void)languages {
    [[ServerManager sharedManager] languagesOnSuccess:^(NSString *response) {
        
        FileManager *fileManager = [[FileManager alloc] init];
        
        [fileManager writeStringToFile:response withName:@"languages.json"];
        
        //NSLog(@"langs response = %@", response);
        
    }
                                          onFailure:^(NSError *error, NSInteger statusCode) {
                                              NSLog(@"error = %@, code = %d", [error localizedDescription], statusCode);
                                          }
     ];
}

#pragma mark - Check internet connection
- (BOOL)connected {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

-(void)notConnectedAlertMessage {
    NSString *title = @"Ошибка сети";
    NSString *message = @"Проверьте интернет соединение";
    NSString *okText = @"OK";
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okButton = [UIAlertAction actionWithTitle:okText style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:okButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - NSUserDefaults
- (void)setObject:(id)object withParamName:(NSString*)paramName
{
    if (object == nil)
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:paramName];
    else
        [[NSUserDefaults standardUserDefaults] setObject:object forKey:paramName];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (id)getObjectWithParamName:(NSString*)paramName
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:paramName];
}

#pragma mark - Custom methods
- (void)expandCatalogDict {
    
    FileManager *fileManager = [[FileManager alloc] init];
    NSString *strResult = [fileManager readStringFromFile:@"catalog.json"];
    //NSLog(@"result from file = %@", strResult);
    
    NSError *jsonError;
    NSData *objectData = [strResult dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:objectData
                                                                    options:NSJSONReadingMutableContainers
                                                                      error:&jsonError];
    
    //NSLog(@"catalog dict items = %@", jsonDict[@"items"]);
    
    NSUInteger catalogId = 1;
    NSUInteger catalog = 1;
    
    for (int i = 0; i < [jsonDict[@"items"] count]; i++) {
        
        [jsonDict[@"items"][i] setObject:@NO forKey:@"statement"];
        
        if ([jsonDict[@"items"][i][@"_catalog"] intValue] == catalog) {
            [jsonDict[@"items"][i] setObject:@(catalogId) forKey:@"catalogId"];
            catalogId++;
        } else {
            catalogId = 1;
            [jsonDict[@"items"][i] setObject:@(catalogId) forKey:@"catalogId"];
            catalogId++;
            catalog++;
        }
    }
    
    [self setObject:jsonDict withParamName:@"catalog"];
    
    //NSLog(@"catalog dict items = %@", jsonDict[@"items"]);
}

- (void)expandStructCatalogDict {
    FileManager *fileManager = [[FileManager alloc] init];
    NSString *strResult = [fileManager readStringFromFile:@"structCatalog.json"];
    //NSLog(@"result from structCatalog file = %@", strResult);
    
    NSError *jsonError;
    NSData *objectData = [strResult dataUsingEncoding:NSUTF8StringEncoding];
    NSMutableDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:objectData
                                                                    options:NSJSONReadingMutableContainers
                                                                      error:&jsonError];
    
    //NSLog(@"struct catalog dict items = %@", jsonDict[@"items"]);
    
    for (int i = 0; i < [jsonDict[@"catalogs"] count]; i++) {
        
        for (int j = 0; j < [jsonDict[@"catalogs"][i][@"items"] count]; j++) {
            
            [jsonDict[@"catalogs"][i][@"items"][j] setObject:@NO forKey:@"statement"];
        }
    }
    
    [self setObject:jsonDict withParamName:@"structCatalog"];
    
    NSLog(@"struct catalog dict catalogs = %@", jsonDict[@"catalogs"]);
}
@end
