//
//  Languages.h
//  Allergy
//
//  Created by Владислав on 11.01.17.
//  Copyright © 2017 Vladislav Popov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Languages : NSObject

- (NSArray*) languagesArray;

@end
