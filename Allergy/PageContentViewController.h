//
//  PageContentViewController.h
//  Allergy
//
//  Created by Владислав on 30.01.17.
//  Copyright © 2017 Vladislav Popov. All rights reserved.
//

#import <UIKit/UIKit.h>


@class PageContentViewController;
@protocol PageContentViewControllerDelegate <NSObject>

-(void)goToScreenRequest:(PageContentViewController*)controller withIndex:(NSUInteger)index;

@end

@interface PageContentViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate> {
    
    UIDatePicker *datePicker;
}

@property (nonatomic,weak) id<PageContentViewControllerDelegate> delegate;

@property (nonatomic,strong) UIPageViewController *PageViewController;


@property  NSUInteger pageIndex;
@property  NSString *imgFile;
@property  NSString *txtTitle;

//@property NSString *firstLabelText;


@property (weak, nonatomic) IBOutlet UIView *screen0;
@property (weak, nonatomic) IBOutlet UIView *screen1;
@property (weak, nonatomic) IBOutlet UIView *screen2;

//@property (weak, nonatomic) IBOutlet UILabel *firstLabel;

//@property (weak, nonatomic) IBOutlet UIImageView *ivScreenImage1;
//@property (weak, nonatomic) IBOutlet UILabel *lblScreenLabel;


//screen0
@property (weak, nonatomic) IBOutlet UIButton *interfaceLangBtn;
- (IBAction)interfaceLangBtnAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *secondLangBtn;
- (IBAction)secondLangBtnAction:(id)sender;


@property (weak, nonatomic) IBOutlet UITableView *interfaceLangTableView;
@property (weak, nonatomic) IBOutlet UITableView *secondLangTableView;


//screen1
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;

@property (weak, nonatomic) IBOutlet UITextField *dateSelectionTextField;

//screen2
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;

@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;

@end
