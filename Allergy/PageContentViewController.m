//
//  PageContentViewController.m
//  Allergy
//
//  Created by Владислав on 30.01.17.
//  Copyright © 2017 Vladislav Popov. All rights reserved.
//

#import "PageContentViewController.h"
#import "Languages.h"
#import "AllergensStatements.h"
#import "ServerManager.h"
#import "FileManager.h"

@interface PageContentViewController ()

//screen0
@property (strong, nonatomic) NSArray *langsArray;
@property (strong, nonatomic) NSMutableArray *langsTitlesArray;

@property (strong, nonatomic) NSDictionary* interfaceLangDict;
@property (strong, nonatomic) NSDictionary* secondLangDict;

@property (weak, nonatomic) IBOutlet UIImageView *interfaceLangImg;
@property (weak, nonatomic) IBOutlet UIImageView *secondLangImg;


@end

@implementation PageContentViewController

//@synthesize lblScreenLabel;
@synthesize pageIndex,imgFile,txtTitle;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //self.ivScreenImage1.image = [UIImage imageNamed:self.imgFile];
    //self.lblScreenLabel.text = self.txtTitle;
    //self.firstLabel.text = self.firstLabelText;
    
    if (pageIndex == 0) {
        self.screen1.hidden = YES;
        self.screen2.hidden = YES;
    } else if (pageIndex == 1) {
        self.screen0.hidden = YES;
        self.screen2.hidden = YES;
    } else if (pageIndex == 2) {
        self.screen0.hidden = YES;
        self.screen1.hidden = YES;
    }
    
    //screen0
    //Languages* langs = [[Languages alloc] init];
    self.langsTitlesArray = [[NSMutableArray alloc] init];
    
    FileManager *fileManager = [[FileManager alloc] init];
    NSString *strResult = [fileManager readStringFromFile:@"languages.json"];
    //NSLog(@"result from languages file = %@", strResult);
    
    NSError *jsonError;
    NSData *objectData = [strResult dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:objectData
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];
    
    self.langsArray = jsonDict[@"languages"];
    
    for (int i = 0; i < [self.langsArray count]; i++) {
        //NSLog(@"title = %@", [itemsArray objectAtIndex:i][@"title"]);
        [self.langsTitlesArray addObject:[self.langsArray objectAtIndex:i][@"title"]];
    }
    
    
    self.interfaceLangTableView.hidden = YES;
    self.secondLangTableView.hidden = YES;
    //screen0 end
    
    //screen1
    datePicker = [[UIDatePicker alloc] init];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [self.dateSelectionTextField setInputView:datePicker];
    
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    [toolBar setTintColor:[UIColor grayColor]];
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(ShowSelectedDate)];
    UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [toolBar setItems:[NSArray arrayWithObjects:space, doneBtn, nil]];
    [self.dateSelectionTextField setInputAccessoryView:toolBar];
    //screen1 end
    
    //screen2
    [self phoneToolbar];
    //screen2 end
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showCardsVC"]) {
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isLogged"];
        
        AllergensStatements *allergensStatements = [[AllergensStatements alloc] init];
        
        [self setObject:allergensStatements.fruitsAndBerriesStatementsArray withParamName:@"fruitsAndBerriesStatements"];
        [self setObject:allergensStatements.vegetablesArray withParamName:@"vegetablesStatements"];
        [self setObject:allergensStatements.cerealsArray withParamName:@"cerealsStatements"];
        [self setObject:allergensStatements.meatAndPoultryArray withParamName:@"meatAndPoultryStatements"];
        [self setObject:allergensStatements.fishAndSeafoodArray withParamName:@"fishAndSeafoodStatements"];
        [self setObject:allergensStatements.herbsAndSpicesArray withParamName:@"herbsAndSpicesStatements"];
        [self setObject:allergensStatements.eggAndMilkProductsArray withParamName:@"eggAndMilkProductsStatements"];
        [self setObject:allergensStatements.nutsAndSeedsArray withParamName:@"nutsAndSeedsStatements"];
        [self setObject:allergensStatements.otherArray withParamName:@"otherStatements"];
        
        [self updateProfile];
        [self profile];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.langsTitlesArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *simpleIdentifier = @"mutualCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleIdentifier];
    }
    
    cell.textLabel.text = [self.langsTitlesArray objectAtIndex:indexPath.row];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView == self.interfaceLangTableView) {
        
        UITableViewCell *cell = [self.interfaceLangTableView cellForRowAtIndexPath:indexPath];
        [self.interfaceLangBtn setTitle:cell.textLabel.text forState:UIControlStateNormal];
        
        //NSLog(@"interface lang id = %@", self.langsArray[indexPath.row][@"_id"]);
        
        self.interfaceLangDict = self.langsArray[indexPath.row];
        
        [self setObject:self.interfaceLangDict withParamName:@"interfaceLangDict"];
        
        self.interfaceLangTableView.hidden = YES;
        
    } else if (tableView == self.secondLangTableView) {
        
        UITableViewCell *cell = [self.secondLangTableView cellForRowAtIndexPath:indexPath];
        [self.secondLangBtn setTitle:cell.textLabel.text forState:UIControlStateNormal];
        
        //NSLog(@"second lang id = %@", self.langsArray[indexPath.row][@"_id"]);
        
        self.secondLangDict = self.langsArray[indexPath.row];
        
        [self setObject:self.secondLangDict withParamName:@"secondLangDict"];
        
        self.secondLangTableView.hidden = YES;
    }
}

#pragma  mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (textField == self.nameTextField) {
        
        [self.nameTextField resignFirstResponder];
        
    } else if (textField == self.emailTextField) {
        
        [self.emailTextField resignFirstResponder];
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField == self.nameTextField) {
        
        [self setObject:self.nameTextField.text withParamName:@"name"];
        
    } else if (textField == self.dateSelectionTextField) {
        
        NSString *dateStr = self.dateSelectionTextField.text;
        
        //приводим к нужному формату, для сохранения потом на сервер
        /*NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        NSDate *date = [formatter dateFromString:dateStr];
        [formatter setDateFormat:@"yyyy/MM/dd"];
        NSString *newFormatString = [formatter stringFromDate:date];
        
        [self setObject:newFormatString withParamName:@"birthday"];*/
        
        [self setObject:self.dateSelectionTextField.text withParamName:@"birthday"];
        
    } else if (textField == self.emailTextField) {
        
        [self setObject:self.emailTextField.text withParamName:@"email"];
        
    } else if (textField == self.phoneTextField) {
        
        [self setObject:self.phoneTextField.text withParamName:@"phone"];
    }
}

#pragma mark - API
-(void)updateProfile {
    
    NSString *name;
    NSString *email;
    NSString *phone;
    NSString *bdate;
    NSUInteger languageId;
    
    if ([self getObjectWithParamName:@"name"] != nil) {
        name = [self getObjectWithParamName:@"name"];
    } else {
        name = @"";
    }
    if ([self getObjectWithParamName:@"email"] != nil) {
        email = [self getObjectWithParamName:@"email"];
    } else {
        email = @"";
    }
    if ([self getObjectWithParamName:@"phone"] != nil) {
        phone = [self getObjectWithParamName:@"phone"];
    } else {
        phone = @"";
    }
    if ([self getObjectWithParamName:@"birthday"] != nil) {
        bdate = [self getObjectWithParamName:@"birthday"];
    } else {
        bdate = @"";
    }
    if ([self getObjectWithParamName:@"secondLangDict"] != nil) {
        languageId = (NSUInteger)[[self getObjectWithParamName:@"secondLangDict"][@"_id"] integerValue];
    } else {
        languageId = 1;
    }
    
    [[ServerManager sharedManager] updateProfileForName:name
                                                  email:email
                                                  phone:phone
                                                  bdate:bdate
                                               language:languageId
                                              onSuccess:^(id response) {
                                                  
                                                  NSString *string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
                                                  NSLog(@"update profile response = %@", string);
                                                  
                                              } onFailure:^(NSError *error, NSInteger statusCode) {
                                                  NSLog(@"error = %@, code = %d", [error localizedDescription], statusCode);
                                              }
     ];
}

-(void)profile {
    [[ServerManager sharedManager] profileOnSuccess:^(NSString *response) {
        
        FileManager *fileManager = [[FileManager alloc] init];
        
        [fileManager writeStringToFile:response withName:@"profile.json"];
        
        NSLog(@"profile response = %@", response);
    }
                                          onFailure:^(NSError *error, NSInteger statusCode) {
                                              NSLog(@"error = %@, code = %d", [error localizedDescription], statusCode);
                                          }
     ];
}

#pragma mark - NSUserDefaults
- (void)setObject:(id)object withParamName:(NSString*)paramName
{
    if (object == nil)
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:paramName];
    else
        [[NSUserDefaults standardUserDefaults] setObject:object forKey:paramName];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (id)getObjectWithParamName:(NSString*)paramName
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:paramName];
}

#pragma mark - Custom methods
//screen1
- (void)ShowSelectedDate {
    /*NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy/MM/dd"];
    //[formatter setDateFormat:@"dd/mm/yyyy"];
    //    [formatter setDateFormat:@"dd/MMM/YYYY hh:min a"];
    
    //[datePicker setLocale: [NSLocale localeWithLocaleIdentifier:@"ru_RU"]];
    NSString *dateStr = [NSString stringWithFormat:@"%@", [formatter stringFromDate:datePicker.date]];
    //[self setObject:datePicker.date withParamName:@"testDate"];
    */
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"ru_RU"]];
    //[dateFormatter setDateFormat:@"EEE, dd MMM yyyy HH:mm"];
//    [dateFormatter setDateFormat:@"dd MMM yyyy"];
    [dateFormatter setDateFormat:@"yyyy/MM/dd"];
    //    [formatter setDateFormat:@"dd/MMM/YYYY hh:min a"];
    
    //[datePicker setLocale: [NSLocale localeWithLocaleIdentifier:@"ru_RU"]];
    NSString *dateStr = [NSString stringWithFormat:@"%@", [dateFormatter stringFromDate:datePicker.date]];
    //[self setObject:datePicker.date withParamName:@"testDate"];

    
    self.dateSelectionTextField.text = dateStr;
    
    //приводим к нужному формату, для сохранения потом на сервер
    /*NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    NSDate *date = [formatter dateFromString:dateStr];
    [formatter setDateFormat:@"yyyy/MM/dd"];
    NSString *newFormatString = [formatter stringFromDate:date];*/
    
    [self setObject:dateStr withParamName:@"birthday"];
    
    [self.dateSelectionTextField resignFirstResponder];
}

//screen2
- (void)phoneToolbar {
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    [numberToolbar setTintColor:[UIColor grayColor]];
    UIBarButtonItem *doneBtn = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)];
    UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    [numberToolbar setItems:[NSArray arrayWithObjects:space, doneBtn, nil]];
    [numberToolbar sizeToFit];
    self.phoneTextField.inputAccessoryView = numberToolbar;
}

-(void)doneWithNumberPad{
    //    NSString *numberFromTheKeyboard = self.phoneTextField.text;
    
    //    [[NSUserDefaults standardUserDefaults] setObject:numberFromTheKeyboard forKey:@"phone"];
    
    [self.phoneTextField resignFirstResponder];
}

#pragma mark - Actions
- (IBAction)interfaceLangBtnAction:(id)sender {
    if (self.interfaceLangTableView.hidden == YES) {
        self.interfaceLangTableView.hidden = NO;
        self.interfaceLangImg.image = [UIImage imageNamed: @"Icon - Down"];
    } else {
        self.interfaceLangTableView.hidden = YES;
        self.interfaceLangImg.image = [UIImage imageNamed: @"Icon - Right"];
    }
}

- (IBAction)secondLangBtnAction:(id)sender {
    if (self.secondLangTableView.hidden == YES) {
        self.secondLangTableView.hidden = NO;
        self.secondLangImg.image = [UIImage imageNamed: @"Icon - Down"];
    } else {
        self.secondLangTableView.hidden = YES;
        self.secondLangImg.image = [UIImage imageNamed: @"Icon - Right"];
    }
}

- (IBAction)goToScreen1:(id)sender {
    
    [self.delegate goToScreenRequest:self withIndex:1];
}

- (IBAction)goToScreen2:(id)sender {
    [self.delegate goToScreenRequest:self withIndex:2];
}



@end
