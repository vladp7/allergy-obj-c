//
//  Product.h
//  Allergy
//
//  Created by Владислав on 10.01.17.
//  Copyright © 2017 Vladislav Popov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Product : NSObject

@property (strong, nonatomic) NSString* productNameEng;
@property (strong, nonatomic) NSString* productNameRus;
@property (strong, nonatomic) NSString* imageName;

@end
