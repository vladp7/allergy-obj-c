//
//  ProfileViewController.m
//  Allergy
//
//  Created by Владислав on 16.01.17.
//  Copyright © 2017 Vladislav Popov. All rights reserved.
//

#import "ProfileViewController.h"
#import "ContactsProfileTableViewCell.h"
#import "ServerManager.h"
#import "FileManager.h"


@interface ProfileViewController () <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSDictionary* profileDict;
@property (strong, nonatomic) NSArray* contactsArray;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *birthdayLabel;
@property (weak, nonatomic) IBOutlet UILabel *choosedAllergensLabel;
@property (weak, nonatomic) IBOutlet UITableView *contactsTableView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contactsTableViewHeightConstraint;

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([[NSUserDefaults standardUserDefaults] stringForKey:@"choosedAllergensString"] != nil) {
        NSString *choosedAllergens = [[NSUserDefaults standardUserDefaults] stringForKey:@"choosedAllergensString"];
        self.choosedAllergensLabel.text = choosedAllergens;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    
    /*
    NSString* filePath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString* catalogFileAtPath = [filePath stringByAppendingPathComponent:@"profile.json"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:catalogFileAtPath]) {
        [self profile];
    }*/
    
    [self profile];
    
    /*FileManager *fileManager = [[FileManager alloc] init];
    NSString *strResult = [fileManager readStringFromFile:@"profile.json"];
    NSLog(@"result from profile file = %@", strResult);
    
    NSError *jsonError;
    NSData *objectData = [strResult dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:objectData
    options:NSJSONReadingMutableContainers
    error:&jsonError];

    self.profileDict = jsonDict[@"data"];
    
    NSLog(@"name = %@", self.profileDict[@"name"]);
     */
    /*for (int i = 0; i < [self.langsArray count]; i++) {
    //NSLog(@"title = %@", [itemsArray objectAtIndex:i][@"title"]);
    [self.langsTitlesArray addObject:[self.langsArray objectAtIndex:i][@"title"]];
    }*/
    
    //if ([[NSUserDefaults standardUserDefaults] objectForKey:@"name"] != nil) {
        //self.nameLabel.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"name"];
    if ([[[NSUserDefaults standardUserDefaults] stringForKey:@"name"] length] > 0) {
        self.nameLabel.text = [[NSUserDefaults standardUserDefaults] objectForKey:@"name"];
    } else {
        self.nameLabel.text = @"Имя не введено";
    }
    
    if ([[[NSUserDefaults standardUserDefaults] stringForKey:@"birthday"] length] > 0) {
        
        NSString *birthday = [[NSUserDefaults standardUserDefaults] objectForKey:@"birthday"];
        //self.birthdayLabel.text = birthday;
        
        NSString *birthDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"birthday"];
        NSDate *todayDate = [NSDate date];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy/MM/dd"];
        int time = [todayDate timeIntervalSinceDate:[dateFormatter dateFromString:birthDate]];
        int allDays = (((time/60)/60)/24);
        int days = allDays%365;
        int years = (allDays-days)/365;
        NSString* yearsString = [@(years) stringValue];
        
        NSString *resultText = [[[birthday stringByAppendingString:@" ("] stringByAppendingString:yearsString] stringByAppendingString:@")"];
        
        self.birthdayLabel.text = resultText;
        
        
        NSLog(@"You live since %i years and %i days",years,days);
        
        //NSLog(@"testDate = %@", [[NSUserDefaults standardUserDefaults] objectForKey:@"testDate"]);
        //NSDate *date = [[NSUserDefaults standardUserDefaults] objectForKey:@"testDate"];
        //NSLog(@"date = %@", date);
    } else {
        self.birthdayLabel.text = @"Дата рождения не введена";
    }
    
    self.contactsArray = [[NSArray alloc] init];
    
    self.contactsArray =  [[NSUserDefaults standardUserDefaults] arrayForKey:@"contacts"];
    
    self.contactsTableViewHeightConstraint.constant = [self.contactsArray count] * self.contactsTableView.rowHeight;
    
    //не используется, оставлен для проверки данных на сервере
    //[self profile];
    
    //не используется, оставлен для проверки данных на сервере
    [self getContacts];
    
    [self.contactsTableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"presentModEPVC"]) {
        
//        EditProfileViewController *vc = [[EditProfileViewController alloc] init];
//        
//        vc.delegate = self;
        
    }
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.contactsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *simpleIdentifier = @"contactCell";
    
    ContactsProfileTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleIdentifier];
    
    if (cell == nil) {
        cell = [[ContactsProfileTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleIdentifier];
    }
    
    NSString *contactName = [[[[NSUserDefaults standardUserDefaults] arrayForKey:@"contacts"] objectAtIndex:indexPath.row] objectForKey:@"contactName"];
    
    NSString *contactPhone = [[[[NSUserDefaults standardUserDefaults] arrayForKey:@"contacts"] objectAtIndex:indexPath.row] objectForKey:@"contactPhone"];
    
    cell.contactName.text = contactName;
    cell.contactPhone.text = contactPhone;
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self performSegueWithIdentifier:@"presentModEPVC" sender:self];
}


#pragma mark - API
//не используется, оставлен для проверки что данные на сервер ушли

-(void)profile {
    [[ServerManager sharedManager] profileOnSuccess:^(NSString *response) {
        
        NSLog(@"profile response = %@", response);
        
    }
                                            onFailure:^(NSError *error, NSInteger statusCode) {
                                                NSLog(@"error = %@, code = %d", [error localizedDescription], statusCode);
                                            }
     ];
}

//не используется, оставлен для проверки что данные на сервер ушли
-(void)getContacts {
    [[ServerManager sharedManager] getContactsOnSuccess:^(id response) {
        
        NSString *string = [[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding];
        NSLog(@"get contacts response = %@", string);
    }
                                              onFailure:^(NSError *error, NSInteger statusCode) {
                                                  NSLog(@"error = %@, code = %d", [error localizedDescription], statusCode);
                                              }
     ];
}

#pragma mark - Actions
- (IBAction)goToEditProfileVC:(id)sender {
    [self performSegueWithIdentifier:@"presentModEPVC" sender:self];
}


- (IBAction)callContactPhone:(id)sender {
    
    UIButton *button = (UIButton *)sender;
    CGPoint center= button.center;
    CGPoint rootViewPoint = [button.superview convertPoint:center toView:self.contactsTableView];
    NSIndexPath *indexPath = [self.contactsTableView indexPathForRowAtPoint:rootViewPoint];
    
    NSString *contactPhone = [[[[NSUserDefaults standardUserDefaults] arrayForKey:@"contacts"] objectAtIndex:indexPath.row] objectForKey:@"contactPhone"];
    
    NSString *draftContactPhone = [[contactPhone componentsSeparatedByCharactersInSet:
                            [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                           componentsJoinedByString:@""];
    
    NSString *tel = [@"tel:" stringByAppendingString:draftContactPhone];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:tel]];
    //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel:+79165986146"]];
    
    NSLog(@"Call action");
}


@end
