//
//  RootViewController.h
//  Allergy
//
//  Created by Владислав on 30.01.17.
//  Copyright © 2017 Vladislav Popov. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PageContentViewController.h"

@interface RootViewController : UIViewController <UIPageViewControllerDataSource, PageContentViewControllerDelegate>


@property (nonatomic,strong) UIPageViewController *PageViewController;
@property (nonatomic,strong) NSArray *ArrScreensCount;

@property  NSUInteger pageIndex;

//@property (nonatomic,strong) NSArray *arrFirstLabelText;

- (PageContentViewController *)viewControllerAtIndex:(NSUInteger)index;

@end
