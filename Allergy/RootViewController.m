//
//  RootViewController.m
//  Allergy
//
//  Created by Владислав on 30.01.17.
//  Copyright © 2017 Vladislav Popov. All rights reserved.
//

#import "RootViewController.h"

@interface RootViewController ()

@end

@implementation RootViewController

@synthesize PageViewController,ArrScreensCount;

- (void)viewDidLoad {
    [super viewDidLoad];
    ArrScreensCount = @[@"",@"",@""];
    
    //arrFirstLabelText = @[@"ЯЗЫК ИНТЕРФЕЙСА",@"ИМЯ",@"EMAIL"];
    
    // Create page view controller
    self.PageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
    self.PageViewController.dataSource = self;
    
    PageContentViewController *startingViewController = [self viewControllerAtIndex:0];
    NSArray *viewControllers = @[startingViewController];
    [self.PageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    // Change the size of page view controller
    //self.PageViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - 30);
    self.PageViewController.view.frame = CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height - 64);
    
    [self addChildViewController:PageViewController];
    [self.view addSubview:PageViewController.view];
    [self.PageViewController didMoveToParentViewController:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Page View Datasource Methods
- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = ((PageContentViewController*) viewController).pageIndex;
    //NSLog(@"index b = %d", index);
    if ((index == 0) || (index == NSNotFound))
    {
        return nil;
    }
    index--;
    //NSLog(@"before");
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = ((PageContentViewController*) viewController).pageIndex;
    //NSLog(@"index a = %d", index);
    if (index == NSNotFound)
    {
        return nil;
    }
    index++;
    if (index == [self.ArrScreensCount count])
    {
        return nil;
    }
    //NSLog(@"after");
    return [self viewControllerAtIndex:index];
}

#pragma mark - Other Methods
- (PageContentViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if (([self.ArrScreensCount count] == 0) || (index >= [self.ArrScreensCount count])) {
        return nil;
    }
    
    // Create a new view controller and pass suitable data.
    PageContentViewController *pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageContentViewController"];
    
    pageContentViewController.delegate = self;
    
    pageContentViewController.txtTitle = self.ArrScreensCount[index];
    //pageContentViewController.firstLabelText = self.arrFirstLabelText[index];
    pageContentViewController.pageIndex = index;
    self.pageIndex = index;
    
    return pageContentViewController;
}

#pragma mark - No of Pages Methods
- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    return [self.ArrScreensCount count];
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    //return 0;
    
    return self.pageIndex;
}

#pragma mark - PageContentViewControllerDelegate
-(void)goToScreenRequest:(PageContentViewController*)controller withIndex:(NSUInteger)index {
    
    PageContentViewController *screen1VC = [self viewControllerAtIndex:index];
    
    NSArray *viewControllers = @[screen1VC];
    
    [self.PageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
}

#pragma mark - Actions
- (IBAction)goToPreviousScreen:(id)sender {
    
    if (self.pageIndex != 0) {
        PageContentViewController *startingViewController = [self viewControllerAtIndex:self.pageIndex - 1];
        NSArray *viewControllers = @[startingViewController];
        [self.PageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionReverse animated:YES completion:nil];
    } else {
        [self dismissViewControllerAnimated:NO completion:nil];
    }
}

@end
