//
//  ServerManager.h
//  Allergy
//
//  Created by Владислав on 01.02.17.
//  Copyright © 2017 Vladislav Popov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ServerManager : NSObject

+ (ServerManager*) sharedManager;


-(void)newSessionOnSuccess:(void(^)(NSString* session)) success
                 onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

-(void)catalogOnSuccess:(void(^)(NSString* response)) success
              onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

-(void)structCatalogOnSuccess:(void(^)(NSString* response)) success
              onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

-(void)languagesOnSuccess:(void(^)(NSString* response)) success
                onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

-(void)updateProfileForName:(NSString*) name
                      email:(NSString*) email
                      phone:(NSString*) phone
                      bdate:(NSString*) bdate
                   language:(NSUInteger) language
                  onSuccess:(void(^)(id response)) success
                  onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;


//не исользутеся, оставлен для проверки данных на сервере
-(void)profileOnSuccess:(void(^)(NSString* response)) success
              onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

-(void)addContactName:(NSString*) contactName
         contactPhone:(NSString*) contactPhone
            onSuccess:(void(^)(NSNumber* contactId)) success
            onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

-(void)getContactsOnSuccess:(void(^)(id response)) success
                  onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

/*
-(void)editContactId:(NSUInteger) contactId
         contactName:(NSString*) contactName
        contactPhone:(NSString*) contactPhone
           onSuccess:(void(^)(id response)) success
           onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;*/

-(void)deleteContactId:(NSUInteger) contactId
             onSuccess:(void(^)(id response)) success
             onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure;

@end
