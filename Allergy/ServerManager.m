//
//  ServerManager.m
//  Allergy
//
//  Created by Владислав on 01.02.17.
//  Copyright © 2017 Vladislav Popov. All rights reserved.
//

#import "ServerManager.h"
#import "AFNetworking.h"

@interface ServerManager ()

@property (strong, nonatomic) AFHTTPSessionManager* sessionManager;
@property (strong, nonatomic) NSString* urlString;
//@property (strong, nonatomic) NSString* token;


@end

@implementation ServerManager

+ (ServerManager*) sharedManager {
    
    static ServerManager* manager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[ServerManager alloc] init];
    });
    
    return manager;
}

- (id)init
{
    self = [super init];
    if (self) {
        
        self.urlString = @"https://bnet.i-partner.ru/projects/Allergy/API/";
        
        self.sessionManager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        self.sessionManager.responseSerializer = [AFHTTPResponseSerializer serializer];
        //[self.sessionManager.requestSerializer setValue:@"rUA0P-hyhpW-xOt" forHTTPHeaderField:@"token"];
        
    }
    return self;
}

-(void)newSessionOnSuccess:(void(^)(NSString* session)) success
                 onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    
    NSDictionary* parametersDictionary = @{@"a":@"new_session"};
    
    [self.sessionManager GET:self.urlString
                   parameters:parametersDictionary
                     progress:nil
                      success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                          NSError* err;
                          NSDictionary* jsonDict = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&err];
                          
                          NSString* user_token = jsonDict[@"token"];
                          
                          [[NSUserDefaults standardUserDefaults] setObject:jsonDict[@"token"] forKey:@"token"];
                          
                          [self.sessionManager.requestSerializer setValue:jsonDict[@"token"] forHTTPHeaderField:@"token"];
                          
                          NSLog(@"newSession response (from ServerManager) token: %@", user_token);
                          
                          if (success) {
                              success(user_token);
                          }
                          
                      }
                      failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                          NSLog(@"error: %@", error);
                          if (failure) {
                              NSHTTPURLResponse *response = error.userInfo[AFNetworkingOperationFailingURLResponseErrorKey];
                              NSInteger statusCode = response.statusCode;
                              
                              failure(error, statusCode);
                          }
                      }
     ];
}

-(void)catalogOnSuccess:(void(^)(NSString* response)) success
              onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure{
    
    NSString *token = [[NSUserDefaults standardUserDefaults] stringForKey:@"token"];
    [self.sessionManager.requestSerializer setValue:token forHTTPHeaderField:@"token"];
    NSDictionary* parametersDictionary = @{@"a":@"catalog"};
    
    [self.sessionManager GET:self.urlString
                  parameters:parametersDictionary
                    progress:nil
                     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                         
                         NSString *string = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                         
                         //NSError* err;
                         //NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&err];
                         
                         //NSData *data = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&err];
                         /*NSData *data = [NSJSONSerialization dataWithJSONObject:responseObject
                                                                            options:NSJSONWritingPrettyPrinted
                                                                              error:&err];
                          */
                         
                         //NSString *dictStr = [dict description];
                         
                         
                         //NSString *str = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&err];
                         
                         //NSLog(@"catalog response (from ServerManager) : %@", dict);
                         
                         //NSString* user_token = dict[@"token"];
                         
                         //NSLog(@"newSession response (from ServerManager) token: %@", user_token);
                         
                         if (success) {
                             success(string);
                         }
                         
                     }
                     failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                         NSLog(@"error: %@", error);
                         if (failure) {
                             NSHTTPURLResponse *response = error.userInfo[AFNetworkingOperationFailingURLResponseErrorKey];
                             NSInteger statusCode = response.statusCode;
                             
                             failure(error, statusCode);
                         }
                     }
     ];
}

-(void)structCatalogOnSuccess:(void(^)(NSString* response)) success
                    onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    
    NSString *token = [[NSUserDefaults standardUserDefaults] stringForKey:@"token"];
    [self.sessionManager.requestSerializer setValue:token forHTTPHeaderField:@"token"];
    NSDictionary* parametersDictionary = @{@"a":@"struct_catalog"};
    
    [self.sessionManager GET:self.urlString
                  parameters:parametersDictionary
                    progress:nil
                     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                         
                         NSString *string = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                         
                         if (success) {
                             success(string);
                         }
                         
                     }
                     failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                         NSLog(@"error: %@", error);
                         if (failure) {
                             NSHTTPURLResponse *response = error.userInfo[AFNetworkingOperationFailingURLResponseErrorKey];
                             NSInteger statusCode = response.statusCode;
                             
                             failure(error, statusCode);
                         }
                     }
     ];
}

-(void)languagesOnSuccess:(void(^)(NSString* response)) success
                onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    
    NSString *token = [[NSUserDefaults standardUserDefaults] stringForKey:@"token"];
    [self.sessionManager.requestSerializer setValue:token forHTTPHeaderField:@"token"];
    NSDictionary* parametersDictionary = @{@"a":@"languages"};
    
    [self.sessionManager GET:self.urlString
                  parameters:parametersDictionary
                    progress:nil
                     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                         
                         NSString *string = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                         
                         if (success) {
                             success(string);
                         }
                         
                     }
                     failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                         NSLog(@"error: %@", error);
                         if (failure) {
                             NSHTTPURLResponse *response = error.userInfo[AFNetworkingOperationFailingURLResponseErrorKey];
                             NSInteger statusCode = response.statusCode;
                             
                             failure(error, statusCode);
                         }
                     }
     ];
}

-(void)updateProfileForName:(NSString*) name
                      email:(NSString*) email
                      phone:(NSString*) phone
                      bdate:(NSString*) bdate
                   language:(NSUInteger) language
                  onSuccess:(void(^)(id response)) success
                  onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {

    NSString *token = [[NSUserDefaults standardUserDefaults] stringForKey:@"token"];
    [self.sessionManager.requestSerializer setValue:token forHTTPHeaderField:@"token"];
    //NSDictionary* parametersDictionary = @{@"a":@"update_profile", @"name":name, @"email":email, @"phone":phone, @"bdate":bdate, @"_language":[NSNumber numberWithUnsignedInteger:language]};
    NSDictionary* parametersDictionary = @{@"name":name, @"email":email, @"phone":phone, @"bdate":bdate, @"_language":[NSNumber numberWithUnsignedInteger:language]};
    
    //[self.sessionManager POST:self.urlString
    [self.sessionManager POST:[self.urlString stringByAppendingString:@"?a=update_profile"]
                   parameters:parametersDictionary
                     progress:nil
                      success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                         
                         //NSString *string = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                         //NSLog(@"updateProfile response (from ServerManager): %@", string);
                         
                         if (success) {
                             success(responseObject);
                         }
                      }
                      failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                         NSLog(@"error: %@", error);
                         if (failure) {
                             NSHTTPURLResponse *response = error.userInfo[AFNetworkingOperationFailingURLResponseErrorKey];
                             NSInteger statusCode = response.statusCode;
                             
                             failure(error, statusCode);
                         }
                      }
     ];
}

 //не используется, т.к. данные хранятся в NSUserDefaults
-(void)profileOnSuccess:(void(^)(NSString* response)) success
                onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    
    NSString *token = [[NSUserDefaults standardUserDefaults] stringForKey:@"token"];
    [self.sessionManager.requestSerializer setValue:token forHTTPHeaderField:@"token"];
    NSDictionary* parametersDictionary = @{@"a":@"profile"};
    
    [self.sessionManager GET:self.urlString
                  parameters:parametersDictionary
                    progress:nil
                     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                         
                         NSString *string = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                         //NSLog(@"profile response (from ServerManager): %@", string);
                         
                         if (success) {
                             success(string);
                         }
                     }
                     failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                         NSLog(@"error: %@", error);
                         if (failure) {
                             NSHTTPURLResponse *response = error.userInfo[AFNetworkingOperationFailingURLResponseErrorKey];
                             NSInteger statusCode = response.statusCode;
                             
                             failure(error, statusCode);
                         }
                     }
     ];
}

-(void)addContactName:(NSString*) contactName
                      contactPhone:(NSString*) contactPhone
                  onSuccess:(void(^)(NSNumber* contactId)) success
                  onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    
    NSString *token = [[NSUserDefaults standardUserDefaults] stringForKey:@"token"];
    [self.sessionManager.requestSerializer setValue:token forHTTPHeaderField:@"token"];
    //NSDictionary* parametersDictionary = @{@"a":@"update_profile", @"name":name, @"email":email, @"phone":phone, @"bdate":bdate, @"_language":[NSNumber numberWithUnsignedInteger:language]};
    NSDictionary* parametersDictionary = @{@"name":contactName, @"phone":contactPhone};
    
    //[self.sessionManager POST:self.urlString
    [self.sessionManager POST:[self.urlString stringByAppendingString:@"?a=add_contact"]
                   parameters:parametersDictionary
                     progress:nil
                      success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                          
                          NSError* err;
                          NSDictionary* jsonDict = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions error:&err];
                          
                          NSNumber* contactId = jsonDict[@"contact_id"];
                          
                          NSString *string = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                          NSLog(@"addContact response (from ServerManager): %@", string);
                          
                          if (success) {
                              success(contactId);
                          }
                      }
                      failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                          NSLog(@"error: %@", error);
                          if (failure) {
                              NSHTTPURLResponse *response = error.userInfo[AFNetworkingOperationFailingURLResponseErrorKey];
                              NSInteger statusCode = response.statusCode;
                              
                              failure(error, statusCode);
                          }
                      }
     ];
}

//не используется, оставлен для првоерки данных на сервере
-(void)getContactsOnSuccess:(void(^)(id response)) success
                  onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    
    NSString *token = [[NSUserDefaults standardUserDefaults] stringForKey:@"token"];
    [self.sessionManager.requestSerializer setValue:token forHTTPHeaderField:@"token"];
    NSDictionary* parametersDictionary = @{@"a":@"get_contacts"};
    
    [self.sessionManager GET:self.urlString
                   parameters:parametersDictionary
                     progress:nil
                      success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                          
                          //NSString *string = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                          //NSLog(@"addContact response (from ServerManager): %@", string);
                          
                          if (success) {
                              success(responseObject);
                          }
                    }
                    failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                          NSLog(@"error: %@", error);
                          if (failure) {
                              NSHTTPURLResponse *response = error.userInfo[AFNetworkingOperationFailingURLResponseErrorKey];
                              NSInteger statusCode = response.statusCode;
                              
                              failure(error, statusCode);
                          }
                    }
     ];
}

//edit_contact обращение к серверу получается не нужно, т.к. у нас не реализовано редактирование контакта, только удаление, или добавление выбором из списка контактов в телефоне.
/*-(void)editContactId:(NSUInteger) contactId
         contactName:(NSString*) contactName
         contactPhone:(NSString*) contactPhone
            onSuccess:(void(^)(id response)) success
            onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    
    NSString *token = [[NSUserDefaults standardUserDefaults] stringForKey:@"token"];
    [self.sessionManager.requestSerializer setValue:token forHTTPHeaderField:@"token"];
    //NSDictionary* parametersDictionary = @{@"a":@"update_profile", @"name":name, @"email":email, @"phone":phone, @"bdate":bdate, @"_language":[NSNumber numberWithUnsignedInteger:language]};
    NSDictionary* parametersDictionary = @{@"id":[NSNumber numberWithUnsignedInteger:contactId], @"name":contactName, @"phone":contactPhone};
    
    //[self.sessionManager POST:self.urlString
    [self.sessionManager POST:[self.urlString stringByAppendingString:@"?a=edit_contact"]
                   parameters:parametersDictionary
                     progress:nil
                      success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                          
                          //NSString *string = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                          //NSLog(@"addContact response (from ServerManager): %@", string);
                          
                          if (success) {
                              success(responseObject);
                          }
                      }
                      failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                          NSLog(@"error: %@", error);
                          if (failure) {
                              NSHTTPURLResponse *response = error.userInfo[AFNetworkingOperationFailingURLResponseErrorKey];
                              NSInteger statusCode = response.statusCode;
                              
                              failure(error, statusCode);
                          }
                      }
     ];
}*/

-(void)deleteContactId:(NSUInteger) contactId
             onSuccess:(void(^)(id response)) success
             onFailure:(void(^)(NSError* error, NSInteger statusCode)) failure {
    
    NSString *token = [[NSUserDefaults standardUserDefaults] stringForKey:@"token"];
    [self.sessionManager.requestSerializer setValue:token forHTTPHeaderField:@"token"];
    NSDictionary* parametersDictionary = @{@"id":[NSNumber numberWithUnsignedInteger:contactId]};
    
    [self.sessionManager POST:[self.urlString stringByAppendingString:@"?a=delete_contact"]
                   parameters:parametersDictionary
                     progress:nil
                      success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                          
                          //NSString *string = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
                          //NSLog(@"addContact response (from ServerManager): %@", string);
                          
                          if (success) {
                              success(responseObject);
                          }
                      }
                        failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                          NSLog(@"error: %@", error);
                          if (failure) {
                              NSHTTPURLResponse *response = error.userInfo[AFNetworkingOperationFailingURLResponseErrorKey];
                              NSInteger statusCode = response.statusCode;
                              
                              failure(error, statusCode);
                          }
                        }
     ];
}

@end
