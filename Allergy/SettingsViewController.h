//
//  SettingsViewController.h
//  Allergy
//
//  Created by Владислав on 11.01.17.
//  Copyright © 2017 Vladislav Popov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : UIViewController {
    //NSMutableArray *visibleRows;
    //NSMutableArray *cellInfo;
}

@property (strong, nonatomic) NSArray * allergensArray;


//top outlets
@property (weak, nonatomic) IBOutlet UIButton *langMenuBtn;
@property (weak, nonatomic) IBOutlet UIButton *allergensMenuBtn;

//top actions
- (IBAction)langMenuBtnAction:(id)sender;
- (IBAction)allergensMenuBtnAction:(id)sender;

//langs container outlet
@property (weak, nonatomic) IBOutlet UIView *langsView;

//langs outlets
@property (weak, nonatomic) IBOutlet UIButton *langBtn;
@property (weak, nonatomic) IBOutlet UILabel *choosedLang;
@property (weak, nonatomic) IBOutlet UIImageView *choosedLangImg;

//langs actions
- (IBAction)langBtnAction:(id)sender;


//allergens container outlet
@property (weak, nonatomic) IBOutlet UIView *allergensView;

//allergens outlets
@property (weak, nonatomic) IBOutlet UIButton *fruitsAndBerriesBtn;
@property (weak, nonatomic) IBOutlet UIButton *vegetablesBtn;
@property (weak, nonatomic) IBOutlet UIButton *cerealsBtn;
@property (weak, nonatomic) IBOutlet UIButton *meatAndPoultryBtn;
@property (weak, nonatomic) IBOutlet UIButton *fishAndSeafoodBtn;
@property (weak, nonatomic) IBOutlet UIButton *herbsAndSpicesBtn;
@property (weak, nonatomic) IBOutlet UIButton *eggAndMilkProductsBtn;
@property (weak, nonatomic) IBOutlet UIButton *nutsAndSeedsBtn;
@property (weak, nonatomic) IBOutlet UIButton *otherBtn;
@property (weak, nonatomic) IBOutlet UIButton *myAllergensBtn;




//allergens actions
- (IBAction)fruitsAndBerriesBtnAction:(id)sender;
- (IBAction)vegetablesBtnAction:(id)sender;
- (IBAction)cerealsBtnAction:(id)sender;
- (IBAction)meatAndPoultryBtnAction:(id)sender;
- (IBAction)fishAndSeafoodBtnAction:(id)sender;
- (IBAction)herbsAndSpicesBtnAction:(id)sender;
- (IBAction)eggAndMilkProductsBtnAction:(id)sender;
- (IBAction)nutsAndSeedsBtnAction:(id)sender;
- (IBAction)otherBtnAction:(id)sender;
- (IBAction)myAllergensBtnAction:(id)sender;



@end
