//
//  SettingsViewController.m
//  Allergy
//
//  Created by Владислав on 11.01.17.
//  Copyright © 2017 Vladislav Popov. All rights reserved.
//

#import "SettingsViewController.h"
#import "Allergens.h"
#import "Allergen.h"
#import "AllergensSettingsTableViewCell.h"
#import "FileManager.h"

@interface SettingsViewController ()  <UITableViewDataSource, UITableViewDelegate> {
//    NSDictionary *allergensDict;
//    NSArray *allergensSectionTitles;
    NSDictionary *structCatalog;
}

@property (strong, nonatomic) NSArray *langsArray;
@property (strong, nonatomic) NSMutableArray * langsTitlesArray;

@property (strong, nonatomic) NSDictionary* secondLangDict;

@property (weak, nonatomic) IBOutlet UIScrollView *allergensScrollView;

@property (weak, nonatomic) IBOutlet UIView *container;

@property (weak, nonatomic) IBOutlet UIView *downView;


//@property (weak, nonatomic) IBOutlet STCollapseTableView *allergensTableView;
//@property (weak, nonatomic) IBOutlet UITableView *allergensTableView;
@property (weak, nonatomic) IBOutlet UITableView *langsTableView;

@property (weak, nonatomic) IBOutlet UITableView *fruitsAndBerriesTableView;
@property (weak, nonatomic) IBOutlet UITableView *vegetablesTableView;
@property (weak, nonatomic) IBOutlet UITableView *cerealsTableView;
@property (weak, nonatomic) IBOutlet UITableView *meatAndPoultryTableView;
@property (weak, nonatomic) IBOutlet UITableView *fishAndSeafoodTableView;
@property (weak, nonatomic) IBOutlet UITableView *herbsAndSpicesTableView;
@property (weak, nonatomic) IBOutlet UITableView *eggAndMilkProductsTableView;
@property (weak, nonatomic) IBOutlet UITableView *nutsAndSeedsTableView;
@property (weak, nonatomic) IBOutlet UITableView *otherTableView;
@property (weak, nonatomic) IBOutlet UITableView *myAllergensTableView;


//buttons containers containts!!!
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *fruitsAndBerriesBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *vegetablesContainerBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cerealsContainerBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *meatAndPoultryBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *fishAndSeafoodBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *herbsAndSpicesBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *eggAndMilkProductsBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nutsAndSeedsBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *otherBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *myAllergensBottomConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *myAllergensTableViewHeightConstraint;


@property (weak, nonatomic) IBOutlet UIImageView *fruitsAndBerriesImg;
@property (weak, nonatomic) IBOutlet UIImageView *vegetablesImg;
@property (weak, nonatomic) IBOutlet UIImageView *cerealsImg;
@property (weak, nonatomic) IBOutlet UIImageView *meatAndPoultryImg;
@property (weak, nonatomic) IBOutlet UIImageView *fishAndSeafoodImg;
@property (weak, nonatomic) IBOutlet UIImageView *herbsAndSpicesImg;
@property (weak, nonatomic) IBOutlet UIImageView *eggAndMilkProductsImg;
@property (weak, nonatomic) IBOutlet UIImageView *nutsAndSeedsImg;
@property (weak, nonatomic) IBOutlet UIImageView *otherImg;
@property (weak, nonatomic) IBOutlet UIImageView *myAllergensImg;


@property (strong, nonatomic) NSMutableArray * fruitsAndBerriesArray;
@property (strong, nonatomic) NSMutableArray * vegetablesArray;
@property (strong, nonatomic) NSMutableArray * cerealsArray;
@property (strong, nonatomic) NSMutableArray * meatAndPoultryArray;
@property (strong, nonatomic) NSMutableArray * fishAndSeafoodArray;
@property (strong, nonatomic) NSMutableArray * herbsAndSpicesArray;
@property (strong, nonatomic) NSMutableArray * eggAndMilkProductsArray;
@property (strong, nonatomic) NSMutableArray * nutsAndSeedsArray;
@property (strong, nonatomic) NSMutableArray * otherArray;
@property (strong, nonatomic) NSMutableArray * myAllergensArray;



//fruitsAndBerries
//vegetables
//cereals
//meatAndPoultry
//fishAndSeafood
//herbsAndSpices
//eggAndMilkProducts
//nutsAndSeeds
//other
//myAllergens


//@property (nonatomic, strong) NSMutableArray* headers;

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.allergensView.hidden = YES;
    
    self.langsTitlesArray = [[NSMutableArray alloc] init];
    
    FileManager *fileManager = [[FileManager alloc] init];
    
    NSString *langsResult = [fileManager readStringFromFile:@"languages.json"];
    //NSLog(@"result from languages file = %@", strResult);
    
    NSError *jsonLangsError;
    NSData *objectLangsData = [langsResult dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *jsonLangsDict = [NSJSONSerialization JSONObjectWithData:objectLangsData
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonLangsError];
    
    self.langsArray = jsonLangsDict[@"languages"];
    
    for (int i = 0; i < [self.langsArray count]; i++) {
        //NSLog(@"title = %@", [itemsArray objectAtIndex:i][@"title"]);
        [self.langsTitlesArray addObject:[self.langsArray objectAtIndex:i][@"title"]];
    }

    
    self.langsTableView.hidden = YES;
//    self.choosedLangImg.image = [UIImage imageNamed: @"Icon - Menu"];
    
    

    /*NSString *strResult = [fileManager readStringFromFile:@"structCatalog.json"];
    //NSLog(@"result from file = %@", strResult);
    
    NSError *jsonError;
    NSData *objectData = [strResult dataUsingEncoding:NSUTF8StringEncoding];
    structCatalog = [NSJSONSerialization JSONObjectWithData:objectData
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];*/

    structCatalog = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"structCatalog"];
//    NSLog(@"jsonDict is = %@", jsonDict);
    
    //NSLog(@"jsonDict catalogs = %@", jsonDict[@"items"][0][@"title"]);
    //NSLog(@"jsonDict catalogs = %@", jsonDict[@"items"]);
    
    self.fruitsAndBerriesArray = [[NSMutableArray alloc] init];
    self.vegetablesArray = [[NSMutableArray alloc] init];
    self.cerealsArray = [[NSMutableArray alloc] init];
    self.meatAndPoultryArray = [[NSMutableArray alloc] init];
    self.fishAndSeafoodArray = [[NSMutableArray alloc] init];
    self.herbsAndSpicesArray = [[NSMutableArray alloc] init];
    self.eggAndMilkProductsArray = [[NSMutableArray alloc] init];
    self.nutsAndSeedsArray = [[NSMutableArray alloc] init];
    self.otherArray = [[NSMutableArray alloc] init];
    
    //NSArray* itemsArray = jsonDict[@"items"];
    NSArray* catalogsArray = structCatalog[@"catalogs"];

    //for (int i = 0; i < [itemsArray count]; i++) {
    for (int i = 0; i < [catalogsArray count]; i++) {
        for (int j = 0; j < [catalogsArray[i][@"items"] count]; j++) {
            switch ([catalogsArray[i][@"items"][j][@"_catalog"] intValue])
            {
                case 1:
                    
                    //[self.fruitsAndBerriesArray addObject:[itemsArray objectAtIndex:i][@"title"]];
                    [self.fruitsAndBerriesArray addObject:[catalogsArray[i][@"items"] objectAtIndex:j][@"title"]];
                    
                    break;
                    
                case 2:
                    
                    //[self.vegetablesArray addObject:[itemsArray objectAtIndex:i][@"title"]];
                    [self.vegetablesArray addObject:[catalogsArray[i][@"items"] objectAtIndex:j][@"title"]];

                    
                    break;
                    
                case 3:
                    
                    //[self.cerealsArray addObject:[itemsArray objectAtIndex:i][@"title"]];
                    [self.cerealsArray addObject:[catalogsArray[i][@"items"] objectAtIndex:j][@"title"]];
                    
                    break;
                    
                case 4:
                    
                    //[self.meatAndPoultryArray addObject:[itemsArray objectAtIndex:i][@"title"]];
                    [self.meatAndPoultryArray addObject:[catalogsArray[i][@"items"] objectAtIndex:j][@"title"]];
                    
                    break;
                    
                case 5:
                    
                    //[self.fishAndSeafoodArray addObject:[itemsArray objectAtIndex:i][@"title"]];
                    [self.fishAndSeafoodArray addObject:[catalogsArray[i][@"items"] objectAtIndex:j][@"title"]];
                    
                    break;
                    
                case 6:
                    
                    //[self.herbsAndSpicesArray addObject:[itemsArray objectAtIndex:i][@"title"]];
                    [self.herbsAndSpicesArray addObject:[catalogsArray[i][@"items"] objectAtIndex:j][@"title"]];
                    
                    break;
                    
                case 7:
                    
                    //[self.eggAndMilkProductsArray addObject:[itemsArray objectAtIndex:i][@"title"]];
                    [self.eggAndMilkProductsArray addObject:[catalogsArray[i][@"items"] objectAtIndex:j][@"title"]];
                    
                    break;
                    
                case 8:
                    
                    //[self.nutsAndSeedsArray addObject:[itemsArray objectAtIndex:i][@"title"]];
                    [self.nutsAndSeedsArray addObject:[catalogsArray[i][@"items"] objectAtIndex:j][@"title"]];
                    
                    break;
                    
                case 9:
                    
                    //[self.otherArray addObject:[itemsArray objectAtIndex:i][@"title"]];
                    [self.otherArray addObject:[catalogsArray[i][@"items"] objectAtIndex:j][@"title"]];
                    
                    break;
                    
                default:
                    
                    break;
            }
        }
    }

    //Allergens* allergens = [[Allergens alloc] init];
    //self.allergensArray = allergens.allergensArray;
    
    self.vegetablesTableView.hidden = YES;
    self.cerealsTableView.hidden = YES;
    self.meatAndPoultryTableView.hidden = YES;
    self.fishAndSeafoodTableView.hidden = YES;
    self.herbsAndSpicesTableView.hidden = YES;
    self.eggAndMilkProductsTableView.hidden = YES;
    self.nutsAndSeedsTableView.hidden = YES;
    self.otherTableView.hidden = YES;
    self.myAllergensTableView.hidden = YES;
    
    self.vegetablesContainerBottomConstraint.constant = 0;
    self.cerealsContainerBottomConstraint.constant = 0;
    self.meatAndPoultryBottomConstraint.constant = 0;
    self.fishAndSeafoodBottomConstraint.constant = 0;
    self.herbsAndSpicesBottomConstraint.constant = 0;
    self.eggAndMilkProductsBottomConstraint.constant = 0;
    self.nutsAndSeedsBottomConstraint.constant = 0;
    self.otherBottomConstraint.constant = 0;
    self.myAllergensBottomConstraint.constant = 0;
    
    
//    [self.allergensTableView reloadData];
}

- (void) viewWillAppear:(BOOL)animated {
    
    if ([[NSUserDefaults standardUserDefaults] dictionaryForKey:@"secondLangDict"] != nil) {
        self.choosedLang.text = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"secondLangDict"][@"title"];
    } else {
        self.choosedLang.text = @"Русский";
    }
    
    
    self.myAllergensArray = [[NSMutableArray alloc] init];
    self.myAllergensArray = [[NSUserDefaults standardUserDefaults] arrayForKey:@"myAllergens"];
    
    self.myAllergensTableViewHeightConstraint.constant = 63 * [self.myAllergensArray count];
    
    [self.myAllergensTableView setEditing:YES animated:YES];
    
    [self.myAllergensTableView reloadData];
    
    if (self.myAllergensTableView.hidden == NO) {
        self.myAllergensBottomConstraint.constant = self.myAllergensTableViewHeightConstraint.constant;
        
        CGFloat contentHeight = self.allergensScrollView.contentSize.height;
        
        CGFloat frameHeight = self.allergensScrollView.frame.size.height;
        
        CGFloat tableHeight = self.myAllergensTableView.contentSize.height;
        
        CGFloat contentOffset = contentHeight + tableHeight - frameHeight;
        
        //[self.allergensScrollView setContentOffset:CGPointMake(0, contentOffset) animated:YES];
        
        [self.allergensScrollView setContentOffset:CGPointMake(0, self.allergensScrollView.contentOffset.y - tableHeight) animated:YES];
        
    } else {
        self.myAllergensBottomConstraint.constant = 0;
    }
    
    if (self.langsView.hidden == NO) {
        self.allergensScrollView.scrollEnabled = NO;
    }
    
//    [self.fruitsAndBerriesTableView reloadData];
//    [self.vegetablesTableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (tableView == self.langsTableView) {
        
        return [self.langsTitlesArray count];
        
    } else if (tableView == self.fruitsAndBerriesTableView) {
        
        return [self.fruitsAndBerriesArray count];
    
    } else if (tableView == self.vegetablesTableView) {
        
        return [self.vegetablesArray count];
        
    } else if (tableView == self.cerealsTableView) {
        
        return [self.cerealsArray count];
        
    } else if (tableView == self.meatAndPoultryTableView) {
        
        return [self.meatAndPoultryArray count];
        
    } else if (tableView == self.fishAndSeafoodTableView) {
        
        return [self.fishAndSeafoodArray count];
        
    } else if (tableView == self.herbsAndSpicesTableView) {
        
        return [self.herbsAndSpicesArray count];
        
    } else if (tableView == self.eggAndMilkProductsTableView) {
        
        return [self.eggAndMilkProductsArray count];
        
    } else if (tableView == self.nutsAndSeedsTableView) {
        
        return [self.nutsAndSeedsArray count];
        
    } else if (tableView == self.otherTableView) {
        
        return [self.otherArray count];
        
    } else if (tableView == self.myAllergensTableView) {
        
        return [self.myAllergensArray count];
        
    } else {
    
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView == self.langsTableView) {
        
        static NSString *simpleIdentifier = @"langCell";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleIdentifier];
        
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleIdentifier];
            
        }
        cell.textLabel.text = [self.langsTitlesArray objectAtIndex:indexPath.row];
        
        return cell;
        
    } else if (tableView == self.fruitsAndBerriesTableView) {
        
        static NSString *simpleIdentifier = @"fruitsAndBerriesCell";
        
        AllergensSettingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleIdentifier];
        
        if (cell == nil) {
            cell = [[AllergensSettingsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleIdentifier];

        }
        
        cell.allergenLabel.text = [self.fruitsAndBerriesArray objectAtIndex:indexPath.row];
        
        if ([structCatalog[@"catalogs"][0][@"items"][indexPath.row][@"statement"] intValue] == 1) {
        //if ([[[[NSUserDefaults standardUserDefaults] arrayForKey:@"fruitsAndBerriesStatements"] objectAtIndex:indexPath.row] intValue] == 1) {
        //if ([self.fruitsAndBerriesArray[indexPath.row][@"statement"] intValue] == 1) {
            //[cell.switchFruitsAndBerries setOn:YES];
            [cell.switchBtnFruitsAndBerries setSelected:YES];
        } else {
            //[cell.switchFruitsAndBerries setOn:NO];
            [cell.switchBtnFruitsAndBerries setSelected:NO];
        }
        
        return cell;
        
    } else if (tableView == self.vegetablesTableView) {
        
        static NSString *simpleIdentifier = @"vegetablesCell";
        
        AllergensSettingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleIdentifier];
        
        if (cell == nil) {
            cell = [[AllergensSettingsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleIdentifier];
            
        }
        
        cell.allergenLabel.text = self.vegetablesArray[indexPath.row];
        
        if ([structCatalog[@"catalogs"][1][@"items"][indexPath.row][@"statement"] intValue] == 1) {
        //if ([[[[NSUserDefaults standardUserDefaults] arrayForKey:@"vegetablesStatements"] objectAtIndex:indexPath.row] intValue] == 1) {
            //[cell.switchVegetables setOn:YES];
            [cell.switchBtnVegetables setSelected:YES];
        } else {
            //[cell.switchVegetables setOn:NO];
            [cell.switchBtnVegetables setSelected:NO];
        }
        
        return cell;
        
    } else if (tableView == self.cerealsTableView) {
        
        static NSString *simpleIdentifier = @"cerealsCell";
        
        AllergensSettingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleIdentifier];
        
        if (cell == nil) {
            cell = [[AllergensSettingsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleIdentifier];
            
        }
        
        cell.allergenLabel.text = [self.cerealsArray objectAtIndex:indexPath.row];
        
        if ([structCatalog[@"catalogs"][2][@"items"][indexPath.row][@"statement"] intValue] == 1) {
        //if ([[[[NSUserDefaults standardUserDefaults] arrayForKey:@"cerealsStatements"] objectAtIndex:indexPath.row] intValue] == 1) {
            //[cell.switchCereals setOn:YES];
            [cell.switchBtnCereals setSelected:YES];
        } else {
            //[cell.switchCereals setOn:NO];
            [cell.switchBtnCereals setSelected:NO];
        }
        
        return cell;
        
    } else if (tableView == self.meatAndPoultryTableView) {
        
        static NSString *simpleIdentifier = @"meatAndPoultryCell";
        
        AllergensSettingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleIdentifier];
        
        if (cell == nil) {
            cell = [[AllergensSettingsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleIdentifier];
        }
        
        cell.allergenLabel.text = [self.meatAndPoultryArray objectAtIndex:indexPath.row];
        
        if ([structCatalog[@"catalogs"][3][@"items"][indexPath.row][@"statement"] intValue] == 1) {
        //if ([[[[NSUserDefaults standardUserDefaults] arrayForKey:@"meatAndPoultryStatements"] objectAtIndex:indexPath.row] intValue] == 1) {
//            [cell.switchMeatAndPoultry setOn:YES];
            [cell.switchBtnMeatAndPoultry setSelected:YES];
        } else {
//            [cell.switchMeatAndPoultry setOn:NO];
            [cell.switchBtnMeatAndPoultry setSelected:NO];
        }
        
        return cell;
        
    } else if (tableView == self.fishAndSeafoodTableView) {
        
        static NSString *simpleIdentifier = @"fishAndSeafoodCell";
        
        AllergensSettingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleIdentifier];
        
        if (cell == nil) {
            cell = [[AllergensSettingsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleIdentifier];
            
        }
        
        cell.allergenLabel.text = [self.fishAndSeafoodArray objectAtIndex:indexPath.row];
        
        if ([structCatalog[@"catalogs"][4][@"items"][indexPath.row][@"statement"] intValue] == 1) {
        //if ([[[[NSUserDefaults standardUserDefaults] arrayForKey:@"fishAndSeafoodStatements"] objectAtIndex:indexPath.row] intValue] == 1) {
            //[cell.switchFishAndSeafood setOn:YES];
            [cell.switchBtnFishAndSeafood setSelected:YES];
        } else {
            //[cell.switchFishAndSeafood setOn:NO];
            [cell.switchBtnFishAndSeafood setSelected:NO];
        }
        
        return cell;
        
    } else if (tableView == self.herbsAndSpicesTableView) {
        
        static NSString *simpleIdentifier = @"herbsAndSpicesCell";
        
        AllergensSettingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleIdentifier];
        
        if (cell == nil) {
            cell = [[AllergensSettingsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleIdentifier];
            
        }
        
        cell.allergenLabel.text = [self.herbsAndSpicesArray objectAtIndex:indexPath.row];
        
        if ([structCatalog[@"catalogs"][5][@"items"][indexPath.row][@"statement"] intValue] == 1) {
        //if ([[[[NSUserDefaults standardUserDefaults] arrayForKey:@"herbsAndSpicesStatements"] objectAtIndex:indexPath.row] intValue] == 1) {
            //[cell.switchHerbsAndSpices setOn:YES];
            [cell.switchBtnHerbsAndSpices setSelected:YES];
        } else {
            //[cell.switchHerbsAndSpices setOn:NO];
            [cell.switchBtnHerbsAndSpices setSelected:NO];
        }
        
        return cell;
        
    } else if (tableView == self.eggAndMilkProductsTableView) {
        
        static NSString *simpleIdentifier = @"eggAndMilkProductsCell";
        
        AllergensSettingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleIdentifier];
        
        if (cell == nil) {
            cell = [[AllergensSettingsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleIdentifier];
            
        }
        
        cell.allergenLabel.text = [self.eggAndMilkProductsArray objectAtIndex:indexPath.row];
        
        if ([structCatalog[@"catalogs"][6][@"items"][indexPath.row][@"statement"] intValue] == 1) {
        //if ([[[[NSUserDefaults standardUserDefaults] arrayForKey:@"eggAndMilkProductsStatements"] objectAtIndex:indexPath.row] intValue] == 1) {
            //[cell.switchEggAndMilkProducts setOn:YES];
            [cell.switchBtnEggAndMilkProducts setSelected:YES];
        } else {
            //[cell.switchEggAndMilkProducts setOn:NO];
            [cell.switchBtnEggAndMilkProducts setSelected:NO];
        }
        
        return cell;
        
    } else if (tableView == self.nutsAndSeedsTableView) {
        
        static NSString *simpleIdentifier = @"nutsAndSeedsCell";
        
        AllergensSettingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleIdentifier];
        
        if (cell == nil) {
            cell = [[AllergensSettingsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleIdentifier];
            
        }
        
        cell.allergenLabel.text = [self.nutsAndSeedsArray objectAtIndex:indexPath.row];
        
        if ([structCatalog[@"catalogs"][7][@"items"][indexPath.row][@"statement"] intValue] == 1) {
        //if ([[[[NSUserDefaults standardUserDefaults] arrayForKey:@"nutsAndSeedsStatements"] objectAtIndex:indexPath.row] intValue] == 1) {
            //[cell.switchNutsAndSeeds setOn:YES];
            [cell.switchBtnNutsAndSeeds setSelected:YES];
        } else {
            //[cell.switchNutsAndSeeds setOn:NO];
            [cell.switchBtnNutsAndSeeds setSelected:NO];
        }
        
        return cell;
        
    } else if (tableView == self.otherTableView) {
        
        static NSString *simpleIdentifier = @"otherCell";
        
        AllergensSettingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleIdentifier];
        
        if (cell == nil) {
            cell = [[AllergensSettingsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleIdentifier];
            
        }
        
        cell.allergenLabel.text = [self.otherArray objectAtIndex:indexPath.row];
        
        if ([structCatalog[@"catalogs"][8][@"items"][indexPath.row][@"statement"] intValue] == 1) {
        //if ([[[[NSUserDefaults standardUserDefaults] arrayForKey:@"otherStatements"] objectAtIndex:indexPath.row] intValue] == 1) {
            //[cell.switchOther setOn:YES];
            [cell.switchBtnOther setSelected:YES];
        } else {
            //[cell.switchOther setOn:NO];
            [cell.switchBtnOther setSelected:NO];
        }
        
        return cell;
        
    } else if (tableView == self.myAllergensTableView && [[NSUserDefaults standardUserDefaults] arrayForKey:@"myAllergens"] != nil) {
        
        static NSString *simpleIdentifier = @"myAllergensCell";
        
        AllergensSettingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleIdentifier];
        
        if (cell == nil) {
            cell = [[AllergensSettingsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleIdentifier];
            
        }
        
        cell.allergenLabel.text = [self.myAllergensArray objectAtIndex:indexPath.row];
        
        if ([[[[NSUserDefaults standardUserDefaults] arrayForKey:@"myAllergensStatements"] objectAtIndex:indexPath.row] intValue] == 1) {
            //[cell.switchMyAllergens setOn:YES];
            [cell.switchBtnMyAllergens setSelected:YES];
        } else {
            //[cell.switchMyAllergens setOn:NO];
            [cell.switchBtnMyAllergens setSelected:NO];
        }
        
        return cell;
        
    } else {
        //этот код не должен быть выполнен, но нужно чтобы было просто else
        static NSString *simpleIdentifier = @"defaultCell";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleIdentifier];
        
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleIdentifier];
            
        }
        
        cell.textLabel.text = @"default text";
        
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView == self.myAllergensTableView && [[NSUserDefaults standardUserDefaults] arrayForKey:@"myAllergens"] != nil && editingStyle == UITableViewCellEditingStyleDelete) {
        
        NSMutableArray *tempAr = [NSMutableArray arrayWithArray: [[NSUserDefaults standardUserDefaults] arrayForKey:@"myAllergens"]];
        
        [tempAr removeObjectAtIndex:indexPath.row];
        
        self.myAllergensArray = tempAr;
        [[NSUserDefaults standardUserDefaults] setObject:tempAr forKey:@"myAllergens"];
        
        NSMutableArray *tempArStatements = [NSMutableArray arrayWithArray: [[NSUserDefaults standardUserDefaults] arrayForKey:@"myAllergensStatements"]];
        
        [tempArStatements removeObjectAtIndex:indexPath.row];
        [[NSUserDefaults standardUserDefaults] setObject:tempArStatements forKey:@"myAllergensStatements"];
        
        [self.myAllergensTableView deleteRowsAtIndexPaths:[NSMutableArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
//        [self.allergensScrollView setContentOffset:CGPointMake(0, self.allergensScrollView.contentOffset.y - self.myAllergensTableView.rowHeight) animated:YES];
        self.myAllergensBottomConstraint.constant = self.myAllergensBottomConstraint.constant - self.myAllergensTableView.rowHeight;
        self.myAllergensTableViewHeightConstraint.constant = self.myAllergensTableViewHeightConstraint.constant - self.myAllergensTableView.rowHeight;
    }
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView == self.langsTableView) {
        
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        
        self.choosedLang.text = [self.langsTitlesArray objectAtIndex:indexPath.row];
        self.langsTableView.hidden = YES;
        self.choosedLangImg.image = [UIImage imageNamed: @"Icon - Right"];
        
        self.secondLangDict = self.langsArray[indexPath.row];
        
        [[NSUserDefaults standardUserDefaults] setObject:self.secondLangDict forKey:@"secondLangDict"];
    }
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.langsTableView) {
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView == self.myAllergensTableView && [[NSUserDefaults standardUserDefaults] arrayForKey:@"myAllergens"] != nil) {
        return UITableViewCellEditingStyleDelete;
    }
    return UITableViewCellEditingStyleNone;
}

//#pragma mark - custom methods

#pragma mark - Actions
- (IBAction)onClose:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)langMenuBtnAction:(id)sender {
    [self.langMenuBtn setTitleColor:[UIColor colorWithRed:255/255 green:255/255 blue:255/255 alpha:1.0] forState:UIControlStateNormal];
    [self.allergensMenuBtn setTitleColor:[UIColor colorWithRed:255/255 green:255/255 blue:255/255 alpha:0.2] forState:UIControlStateNormal];
    self.langsView.hidden = NO;
    self.allergensView.hidden = YES;
    
    NSInteger wd = self.langsView.frame.origin.y;
    NSInteger ht = self.langsView.frame.size.height;
    
    float sizeOfContent = wd+ht;
    
    //self.allergensScrollView.contentSize = CGSizeMake(self.allergensScrollView.frame.size.width, sizeOfContent+25);
    //self.allergensScrollView.contentSize = CGSizeMake(self.allergensScrollView.frame.size.width, 100);
    
    self.downView.hidden = NO;
    self.allergensScrollView.scrollEnabled = NO;
}

- (IBAction)allergensMenuBtnAction:(id)sender {
    [self.allergensMenuBtn setTitleColor:[UIColor colorWithRed:255/255 green:255/255 blue:255/255 alpha:1.0] forState:UIControlStateNormal];
    [self.langMenuBtn setTitleColor:[UIColor colorWithRed:255/255 green:255/255 blue:255/255 alpha:0.2] forState:UIControlStateNormal];
    self.allergensView.hidden = NO;
    self.langsView.hidden = YES;
    
    NSInteger wd = self.allergensView.frame.origin.y;
    NSInteger ht = self.allergensView.frame.size.height;
    
    float sizeOfContent = wd+ht;
    
    //self.allergensScrollView.contentSize = CGSizeMake(self.allergensScrollView.frame.size.width, sizeOfContent+25);
    
    self.downView.hidden = YES;
    self.allergensScrollView.scrollEnabled = YES;
}

- (IBAction)langBtnAction:(id)sender {
    if (self.langsTableView.hidden == YES) {
        self.langsTableView.hidden = NO;
        self.choosedLangImg.image = [UIImage imageNamed: @"Icon - Down"];
    } else {
        self.langsTableView.hidden = YES;
        self.choosedLangImg.image = [UIImage imageNamed: @"Icon - Right"];
    }
}
- (IBAction)fruitsAndBerriesBtnAction:(id)sender {
    if (self.fruitsAndBerriesTableView.hidden == YES) {
        self.fruitsAndBerriesTableView.hidden = NO;
        self.fruitsAndBerriesBottomConstraint.constant = self.fruitsAndBerriesTableView.frame.size.height;
        self.fruitsAndBerriesImg.image = [UIImage imageNamed: @"Icon - Down"];
    } else {
        self.fruitsAndBerriesTableView.hidden = YES;
        self.fruitsAndBerriesBottomConstraint.constant = 0;
        self.fruitsAndBerriesImg.image = [UIImage imageNamed: @"Icon - Right"];
    }
}

- (IBAction)vegetablesBtnAction:(id)sender {
    if (self.vegetablesTableView.hidden == YES) {
        self.vegetablesTableView.hidden = NO;
        self.vegetablesContainerBottomConstraint.constant = self.vegetablesTableView.frame.size.height;
        self.vegetablesImg.image = [UIImage imageNamed: @"Icon - Down"];
    } else {
        self.vegetablesTableView.hidden = YES;
        self.vegetablesContainerBottomConstraint.constant = 0;
        self.vegetablesImg.image = [UIImage imageNamed: @"Icon - Right"];
    }
}

- (IBAction)cerealsBtnAction:(id)sender {
    if (self.cerealsTableView.hidden == YES) {
        self.cerealsTableView.hidden = NO;
        self.cerealsContainerBottomConstraint.constant = self.cerealsTableView.frame.size.height;
        self.cerealsImg.image = [UIImage imageNamed: @"Icon - Down"];
    } else {
        self.cerealsTableView.hidden = YES;
        self.cerealsContainerBottomConstraint.constant = 0;
        self.cerealsImg.image = [UIImage imageNamed: @"Icon - Right"];
    }
}

- (IBAction)meatAndPoultryBtnAction:(id)sender {
    if (self.meatAndPoultryTableView.hidden == YES) {
        self.meatAndPoultryTableView.hidden = NO;
        self.meatAndPoultryBottomConstraint.constant = self.meatAndPoultryTableView.frame.size.height;
        self.meatAndPoultryImg.image = [UIImage imageNamed: @"Icon - Down"];
    } else {
        self.meatAndPoultryTableView.hidden = YES;
        self.meatAndPoultryBottomConstraint.constant = 0;
        self.meatAndPoultryImg.image = [UIImage imageNamed: @"Icon - Right"];
    }
}

- (IBAction)fishAndSeafoodBtnAction:(id)sender {
    if (self.fishAndSeafoodTableView.hidden == YES) {
        self.fishAndSeafoodTableView.hidden = NO;
        self.fishAndSeafoodBottomConstraint.constant = self.fishAndSeafoodTableView.frame.size.height;
        self.fishAndSeafoodImg.image = [UIImage imageNamed: @"Icon - Down"];
    } else {
        self.fishAndSeafoodTableView.hidden = YES;
        self.fishAndSeafoodBottomConstraint.constant = 0;
        self.fishAndSeafoodImg.image = [UIImage imageNamed: @"Icon - Right"];
    }
}

- (IBAction)herbsAndSpicesBtnAction:(id)sender {
    if (self.herbsAndSpicesTableView.hidden == YES) {
        self.herbsAndSpicesTableView.hidden = NO;
        self.herbsAndSpicesBottomConstraint.constant = self.herbsAndSpicesTableView.frame.size.height;
        self.herbsAndSpicesImg.image = [UIImage imageNamed: @"Icon - Down"];
    } else {
        self.herbsAndSpicesTableView.hidden = YES;
        self.herbsAndSpicesBottomConstraint.constant = 0;
        self.herbsAndSpicesImg.image = [UIImage imageNamed: @"Icon - Right"];
    }
}

- (IBAction)eggAndMilkProductsBtnAction:(id)sender {
    if (self.eggAndMilkProductsTableView.hidden == YES) {
        self.eggAndMilkProductsTableView.hidden = NO;
                self.eggAndMilkProductsBottomConstraint.constant = self.eggAndMilkProductsTableView.frame.size.height;
        self.eggAndMilkProductsImg.image = [UIImage imageNamed: @"Icon - Down"];
    } else {
        self.eggAndMilkProductsTableView.hidden = YES;
        self.eggAndMilkProductsBottomConstraint.constant = 0;
        self.eggAndMilkProductsImg.image = [UIImage imageNamed: @"Icon - Right"];
    }
}

- (IBAction)nutsAndSeedsBtnAction:(id)sender {
    if (self.nutsAndSeedsTableView.hidden == YES) {
        self.nutsAndSeedsTableView.hidden = NO;
        self.nutsAndSeedsBottomConstraint.constant = self.nutsAndSeedsTableView.frame.size.height;
        self.nutsAndSeedsImg.image = [UIImage imageNamed: @"Icon - Down"];
    } else {
        self.nutsAndSeedsTableView.hidden = YES;
        self.nutsAndSeedsBottomConstraint.constant = 0;
        self.nutsAndSeedsImg.image = [UIImage imageNamed: @"Icon - Right"];
    }
}

- (IBAction)otherBtnAction:(id)sender {
    if (self.otherTableView.hidden == YES) {
        self.otherTableView.hidden = NO;
        self.otherBottomConstraint.constant = self.otherTableView.frame.size.height;
        self.otherImg.image = [UIImage imageNamed: @"Icon - Down"];
    } else {
        self.otherTableView.hidden = YES;
        self.otherBottomConstraint.constant = 0;
        self.otherImg.image = [UIImage imageNamed: @"Icon - Right"];
    }
}

- (IBAction)myAllergensBtnAction:(id)sender {
    if (self.myAllergensTableView.hidden == YES) {
        self.myAllergensTableView.hidden = NO;
        self.myAllergensBottomConstraint.constant = self.myAllergensTableView.frame.size.height;
        
//        CGFloat contentHeight = self.allergensScrollView.contentSize.height;
        
//        CGFloat frameHeight = self.allergensScrollView.frame.size.height;
       
        CGFloat tableHeight = self.myAllergensTableView.contentSize.height;
        
//        CGFloat contentOffset = contentHeight + tableHeight - frameHeight;
        
//        [self.allergensScrollView setContentOffset:CGPointMake(0, contentOffset) animated:YES];
        
        
        
        //[self.allergensScrollView setContentOffset:CGPointMake(0, self.allergensScrollView.contentOffset.y + tableHeight) animated:YES];
        
        [self.allergensScrollView setContentOffset:CGPointMake(0, self.allergensScrollView.contentOffset.y + tableHeight) animated:YES];
        self.myAllergensImg.image = [UIImage imageNamed: @"Icon - Down"];
        
    } else {
        self.myAllergensTableView.hidden = YES;
        self.myAllergensBottomConstraint.constant = 0;
        self.myAllergensImg.image = [UIImage imageNamed: @"Icon - Right"];
    }
}


@end
