//
//  UserDefaults.h
//  Allergy
//
//  Created by Владислав on 03.02.17.
//  Copyright © 2017 Vladislav Popov. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserDefaults : NSObject

- (void)setObject:(id)object withParamName:(NSString*)paramName;
- (id)getObjectWithParamName:(NSString*)paramName;
- (NSArray *)getArrayWithParamName:(NSString*)paramName;
- (NSString *)getStringWithParamName:(NSString*)paramName;
- (NSDictionary<NSString *, id> *)getDictionaryWithParamName:(NSString*)paramName;

@end
