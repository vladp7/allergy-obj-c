//
//  UserDefaults.m
//  Allergy
//
//  Created by Владислав on 03.02.17.
//  Copyright © 2017 Vladislav Popov. All rights reserved.
//

#import "UserDefaults.h"

@implementation UserDefaults

- (void)setObject:(id)object withParamName:(NSString*)paramName {
    
    if (object == nil)
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:paramName];
    else
        [[NSUserDefaults standardUserDefaults] setObject:object forKey:paramName];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (id)getObjectWithParamName:(NSString*)paramName {
    
    return [[NSUserDefaults standardUserDefaults] objectForKey:paramName];
}


- (NSArray *)getArrayWithParamName:(NSString*)paramName {
    
    return [[NSUserDefaults standardUserDefaults] arrayForKey:paramName];
}

- (NSString *)getStringWithParamName:(NSString*)paramName {
    
    return [[NSUserDefaults standardUserDefaults] stringForKey:paramName];
}

- (NSDictionary<NSString *, id> *)getDictionaryWithParamName:(NSString*)paramName {
    
    return [[NSUserDefaults standardUserDefaults] dictionaryForKey:paramName];
}

@end
