//
//  ViewController.m
//  Allergy
//
//  Created by Владислав on 09.01.17.
//  Copyright © 2017 Vladislav Popov. All rights reserved.
//

#import "ViewController.h"
#import "MenuTableViewCell.h"
#import "FileManager.h"

@interface ViewController () <UITableViewDataSource, UITableViewDelegate> {
    __weak IBOutlet UITableView *menuTableView;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    menuTableView.dataSource = self;
    menuTableView.delegate = self;
    
    self.menuArray = @[@"Аллергия", @"Профиль", @"О проекте"];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.menuArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *SimpleIdentifier = @"menuCell";
    
    MenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:SimpleIdentifier];
    
    if (cell == nil) {
        cell = [[MenuTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:SimpleIdentifier];
    }
    
//    UIImage *image = [UIImage imageNamed:self.menuImageArray[indexPath.row]];
//    cell.imageView.image = image;
    
//    cell.textLabel.text = self.menuArray[indexPath.row];
    cell.menuItemLabel.text = self.menuArray[indexPath.row];
//    cell.textLabel.text = self.menuArray[indexPath.row];
//    cell.textLabel.textColor = [UIColor colorWithRed:77.0f/255.0f green:103.0f/255.0f blue:240.0f/255.0f alpha:1.0f];
//    cell.textLabel.font = [UIFont fontWithName:@"GothamPro-Medium" size:31];
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (indexPath.row == 0) {
//        [self performSegueWithIdentifier:@"showCards" sender:self];
//    }
    switch (indexPath.row) {
        case 0:
            [self performSegueWithIdentifier:@"showCards" sender:self];
            break;
            
//        case 1:
//            [self performSegueWithIdentifier:@"showOfferCard" sender:self];
//            break;
            
        case 1:
            [self performSegueWithIdentifier:@"showMyMedCard" sender:self];
            break;
            
        case 2:
            [self performSegueWithIdentifier:@"showAboutProject" sender:self];
            break;
            
//        case 4:
//            [self performSegueWithIdentifier:@"showIntro" sender:self];
//            break;
            
        default:
            break;
    }
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"exitSegue"]) {
        
        //удалить все ключи
        NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
        [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
        
        //FileManager *fileManager = [[FileManager alloc] init];
        //[fileManager deleteFile:@"profile.json"];
    }
}

#pragma mark - Actions
- (IBAction)onClose:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)exitAction:(id)sender {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"isLogged"];
}

- (IBAction)facebook:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.facebook.com"]];
}

- (IBAction)twitter:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.twitter.com"]];
}

- (IBAction)vk:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.vk.com"]];
}

@end
